#include <string>
#include <sstream>
#include "generic.h"

namespace olcfmemres {
  std::string binary(unsigned long long v) {
    std::stringstream lSS;
    for (int lI=63 ; lI>=0 ; lI--) {
      lSS << ((v & (1LLU << lI)) != 0 ? 1 : 0);
    }
    return (lSS.str());
  }
}
