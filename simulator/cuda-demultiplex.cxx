// Christos: CUDA mem system simulation

#include "cuda-demultiplex.h"

namespace olcfmemres {
  
  ThreadBlockAccessDemultiplexingReader::
  ThreadBlockAccessDemultiplexingReader(size_t aNumOfThreads, const char* aTrace) {
    mCudaReaders = new gleipnir::CudaReader*[aNumOfThreads];
    mReaders = new ThreadSpecificReader*[aNumOfThreads];
    for (size_t lI=0 ; lI<aNumOfThreads ; ++lI) {
      mCudaReaders[lI] = new gleipnir::CudaReader(aTrace);
      mReaders[lI] = new ThreadSpecificReader(PForThread(lI), *mCudaReaders[lI]);
    }
    mNumberOfThreads = aNumOfThreads;
    //      mPreviousCycle = 0LLU;
    //      mCycleCount = 0LLU;
    mHasNext = false;
    mAdvanced=false;
  }

  bool ThreadBlockAccessDemultiplexingReader::hasNext() {
    if (!mAdvanced) {
      advance();
    }
    return (mNext.mActiveWarps != std::bitset<MAX_NUM_OF_WARPS>(0));
  }
  
  const ThreadBlockAccessDemultiplexingReader::Record& ThreadBlockAccessDemultiplexingReader::next() {
    if (!hasNext()) {
      throw ENoMoreRecords();
    }
    mAdvanced=false;
    return (mNext);
  }
  
  void ThreadBlockAccessDemultiplexingReader::advance() {      
    mAdvanced=true;
    mNext.mActiveWarps.reset();
    
    /* We need to compute which threads are active in this cycle. A group of
     * threads are active if they perform the same type of access (load/store)
     * against the same type of memory in the same cycle.
     */
    
    /* Records may exhibit the following behavior:
     * (1) threads may or may not have an entry (if not all threads cause the same
     *     amount of accesses), and
     * (2) threads may have caused those accesses at different cycles.
     *
     * For the later, we need to determine which is the next cycle (cycles in records
     * are incremental but not equally spaced due to the way we trace branching). The
     * next cycle is defined as the cycle that is closest to the last one.
       */
    
    /* At the end of what is about to happen, those set bit indices in "mActiveThreads"
     * are the thread indices active in this cycle that can be safely grabbed from 
     * mBufferedRecords. 
     */
    for (size_t lWarpIdx=0 ; lWarpIdx<(mNumberOfThreads/WARP_SIZE) ; lWarpIdx++) {
      WarpRecord& lWarpRecord = mNext.mRecords[lWarpIdx];
      lWarpRecord.mActiveThreads.reset();
      
      unsigned long long& lNewCycle = lWarpRecord.mCycle;
      bool lFirst = true;
      
      for (size_t lThreadId=lWarpIdx*WARP_SIZE ; lThreadId<(lWarpIdx+1)*WARP_SIZE ; ++lThreadId) {
	ThreadSpecificReader& lReader = *mReaders[lThreadId];
	if (lReader.hasNext()) {
	  unsigned long long lCycle = lReader.peek().getCycle();
	  lNewCycle = lFirst ? lCycle : std::min(lNewCycle, lCycle);
	  lFirst = false;
	}
      }
      
      if (lFirst) // end of stream!
	continue;
      
      gleipnir::AccessTypes::Value  lAccType = gleipnir::AccessTypes::UNKNOWN;
      gleipnir::SegmentTypes::Value lSegType = gleipnir::SegmentTypes::UNKNOWN;
      size_t lPayLoadSize=0;
      lFirst = true;
	
      for (size_t lThreadId=lWarpIdx*WARP_SIZE ; lThreadId<(lWarpIdx+1)*WARP_SIZE ; ++lThreadId) {
	ThreadSpecificReader& lReader = *mReaders[lThreadId];
	if (!lReader.hasNext())
	  continue;
	  
	const gleipnir::Record& lRecord = lReader.peek();
	
	if (lRecord.getCycle()!=lNewCycle)
	  continue;
	
	if (lFirst) {
	  lAccType = lRecord.getAccessType();
	  lSegType = lRecord.getSegmentType();
	  lPayLoadSize = lRecord.getSize();
	}

	if (lRecord.getAccessType()==lAccType && lRecord.getSegmentType()==lSegType) {
	  lWarpRecord.mActiveThreads.set(lThreadId % WARP_SIZE);
	  lWarpRecord.mBufferedRecords[lThreadId % WARP_SIZE] = lRecord;
	  if (lRecord.getSize() != lPayLoadSize) {
	    throw ENonFixedSizePayload();
	  }
	  lReader.next();
	}
	lFirst = false;
      }
      
      if (!lFirst) {     // this warp is active
	mNext.mActiveWarps.set(lWarpIdx);
	lWarpRecord.mAccessType = lAccType;
	lWarpRecord.mSegmentType = lSegType;
	lWarpRecord.mPayloadSize = lPayLoadSize;
      }

      //      std::cout << lNewCycle << ": " << lWarpRecord.mActiveThreads.to_string() << std::endl;
    }
  }
}
