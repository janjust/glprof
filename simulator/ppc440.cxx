// Christos: some code I have been playing with..

#include <iomanip>
#include <iostream>
#include <sstream>

#include "ppc440.h"
#include "parser.h"


/** this is from the HERCULES sources */
template<typename V_, typename ITERATOR_, typename T_>
class TransformIterator : public std::iterator<std::input_iterator_tag, V_>{
  protected:
    T_        mTransformationFunctor;
    ITERATOR_ mCurrent;
  public:
    TransformIterator(const ITERATOR_& aIterator)
      : mCurrent(aIterator)
    { }

    TransformIterator<V_, ITERATOR_, T_>& operator++() {
      mCurrent++;
      return (*this);
    }

    TransformIterator<V_, ITERATOR_, T_> operator++(int) {
      TransformIterator<V_, ITERATOR_, T_> lTmp(mCurrent);
      operator++();
      return (lTmp);
    }

    const V_& operator*() const { return (mTransformationFunctor(*mCurrent)); }
    bool operator==(const TransformIterator<V_, ITERATOR_, T_>& aI) const {
      return (mCurrent == aI.mCurrent);
    }
    bool operator!=(const TransformIterator<V_, ITERATOR_, T_>& aI) const {
      return (!operator==(aI));
    }
};

namespace olcfmemres {

  bool PPC440::understands(unsigned int aEventMask) {
    unsigned int lBoth = gleipnir::AccessTypes::LOAD|gleipnir::AccessTypes::STORE;
    return ((aEventMask | lBoth) == lBoth);
  }

  FeedQuickStatus::Value PPC440::feed(gleipnir::AccessTypes::Value aAccess, uintptr_t aEA, size_t aPayload, size_t aTimeStamp) {
    FeedQuickStatus::Value lReturnStatus = 0;
    bool lIsLoad = aAccess==gleipnir::AccessTypes::LOAD;
    size_t lSetId = setFrom(aEA);  // effective address to set
    size_t lAT  = addressTag(aEA); // effective address to way 

    if (DEBUG) {
      std::cout << ((void*) aEA) << " " << binary(aEA) << " " << lSetId << " " << lAT << std::endl;
    }

    Set& lSet = mSets[lSetId];
    Statistics& lStatistics = dataFor(aEA).counters();

    if (mNumberOfAccesses==0) {
      lSet.mPreviousAddress = aEA;
    }

    lSet.mMedianAddrDiff << uintptr_abs_diff(lSet.mPreviousAddress, aEA);

    std::map<uintptr_t, size_t>::iterator lAddressTagLookup = lSet.mAddressTagToWay.find(lAT);    
    bool lIsHit = lAddressTagLookup!=lSet.mAddressTagToWay.end();

    lSet.mNumberOfAccesses++;
    mNumberOfAccesses++;

    lSet.mCounters.accessedAt(aTimeStamp);
    lStatistics.accessedAt(aTimeStamp);

    lSet.mCounters.spatialLocalityTracker().feed(aEA);
    lStatistics.spatialLocalityTracker().feed(aEA);

    if (lIsHit) { // hit
      lReturnStatus |= FeedQuickStatus::HIT;
      Set::LineInfo& lLine = lSet.mArray[lSet.mAddressTagToWay[lAT]];
      lLine.mTouched++;

      Statistics::IntegerDataKey lEvent = lIsLoad ? Statistics::LOAD_HIT : Statistics::STORE_HIT;
      lSet.mCounters[lEvent]++;
      lStatistics[lEvent]++;

      if (!lIsLoad && lLine.mLastEventWasStore) {
        lReturnStatus |= FeedQuickStatus::WAW;
        lSet.mCounters[Statistics::WAW]++;
        lStatistics[Statistics::WAW]++;
      }

      lLine.mLastEventWasStore = !lIsLoad;

      if (DEBUG) {
        std::cout << "(V) hit: " << ((void*) aEA)
          << " @ " << lSetId << ":" << lSet.mAddressTagToWay[lAT] << std::endl;
      }
    } else { // miss
      lReturnStatus |= FeedQuickStatus::MISS;
      Statistics::IntegerDataKey lEvent = lIsLoad ? Statistics::LOAD_MISS : Statistics::STORE_MISS;

      lSet.mCounters[lEvent]++;
      lStatistics[lEvent]++;

      if (DEBUG) {
        std::cout << "(V) miss" << std::endl;
      }
      lSet.mLastWayUsed = (++lSet.mLastWayUsed) % NWAYS;

      Set::LineInfo& lLine = lSet.mArray[lSet.mLastWayUsed];

      if (lLine.mIsSet) { // evict
        lReturnStatus |= FeedQuickStatus::EVICTION;
        if (DEBUG) {
          uintptr_t lEvictedEA = 
            (((uintptr_t) lSet.mArray[lSet.mLastWayUsed].mAddressTag) << SET_ADDRESS_OFFSET) | (((uintptr_t) lSetId) << SET_ADDRESS_OFFSET);
          std::cout << "(V) evicted line with address " << lEvictedEA << std::endl;
        }
        lSet.mMedianTouched << lLine.mTouched;
        lLine.mTouched=0;

        lLine.mIsSet = 0;
        lLine.mLastEventWasStore = 0;
        uintptr_t lVictimEA = lSet.mArray[lSet.mLastWayUsed].mAddressTag;
        lSet.mAddressTagToWay.erase(lVictimEA);
        lSet.mCounters[Statistics::EVICTION]++;

        //	dataFor(lVictimEA).counters()[IntegerDataKeyEVICTION]++;
      }
      lSet.mArray[lSet.mLastWayUsed].mIsSet = 1;
      lSet.mAddressTagToWay[lAT] = lSet.mLastWayUsed;
      lSet.mArray[lSet.mLastWayUsed].mAddressTag = lAT;
      lSet.mArray[lSet.mLastWayUsed].mLastEventWasStore = !lIsLoad;
    }

    lSet.mPreviousAddress = aEA;
    return (lReturnStatus);
  }

  double PPC440::read(const SetSelect& aSet, Statistics::RealDataKey aId) const {
    return (mSets[aSet.index()].mCounters.read(aId));
  }

  unsigned long long PPC440::read(const SetSelect& aSet, Statistics::IntegerDataKey aId) const {
    return (mSets[aSet.index()].mCounters.read(aId));
  }

  unsigned long long PPC440::read(const AllSets& aAllSets, Statistics::IntegerDataKey aId) const {
    return (read0<Statistics::IntegerDataKey, unsigned long long>(aAllSets, aId));
#if 0
    unsigned long long lTotal = 0, lValueMin=~0LLU, lValueMax=0;
    for (size_t lSetIndex=0 ; lSetIndex<NSETS ; lSetIndex++) {
      unsigned long long lValue = read(SetSelect(lSetIndex), aId);
      lValueMin = std::min(lValueMin, lValue);
      lValueMax = std::max(lValueMax, lValue);
      lTotal += lValue;
    }
    switch (aAllSets.mode()) {
      case AllSets::MINIMUM: { return (lValueMin); }
      case AllSets::MAXIMUM:  {return (lValueMax); }
      case AllSets::TOTAL: { return (lTotal); }
      case AllSets::AVERAGE: { return (lTotal / NSETS); }
    }
#endif
  }

  double PPC440::read(const AllSets& aAllSets, Statistics::RealDataKey aId) const {
    return (read0<Statistics::RealDataKey, double>(aAllSets, aId));
  }

  void PPC440::dump() {
    size_t lWidth = 6;

    std::cout << std::left << std::setw(15) << "" << "Sets " << std::endl;
    std::cout << std::left << std::setw(15) << "Metric" << std::right;
    for (size_t lI=0 ; lI<NSETS ; lI++) {
      std::cout << std::setw(lWidth) << lI;
    }
    std::cout << std::endl;

    const size_t lIntItems = 5;
    std::string lIntLegend[] = { 
      "Load Hits", "Load Misses", "Store Hits", "Store Misses", "Access Delay" };
    Statistics::IntegerDataKey lIntDataKeys[] = { 
      Statistics::LOAD_HIT, Statistics::LOAD_MISS, Statistics::STORE_HIT, Statistics::STORE_MISS,
      Statistics::TIME_UNITS_ELAPSED_SINCE_LAST_ACCESS
    };

    for (size_t lItemIndex=0 ; lItemIndex<lIntItems ; lItemIndex++) {
      Statistics::IntegerDataKey lIntDataKey = lIntDataKeys[lItemIndex];
      std::cout << std::left << std::setw(15) << (lIntLegend[lItemIndex] + ":") << std::right;
      for (size_t lI=0 ; lI<NSETS ; lI++) {
        std::cout << std::setw(lWidth) << read(SetSelect(lI), lIntDataKey);
      }
      std::cout << " (tot: " << read(AllSets(AllSets::TOTAL), lIntDataKey)
        << ", min/avg/max: " << read(AllSets(AllSets::MINIMUM), lIntDataKey)
        << "/" << read(AllSets(AllSets::AVERAGE), lIntDataKey)
        << "/" << read(AllSets(AllSets::MAXIMUM), lIntDataKey)
        << ")" << std::endl;
    }

    const size_t lFltItems = 1;
    std::string lFltLegend[] = { "Access Freq." };
    Statistics::RealDataKey lFltDataKeys[] = {
      Statistics::ACCESS_FREQUENCY
    };

    for (size_t lItemIndex=0 ; lItemIndex<lFltItems ; lItemIndex++) {
      Statistics::RealDataKey lRealDataKey = lFltDataKeys[lItemIndex];
      std::cout << std::left << std::setw(15) << (lFltLegend[lItemIndex] + ":") << std::right;
      for (size_t lI=0 ; lI<NSETS ; lI++) {
        std::cout << std::setw(lWidth) << std::setprecision(2) << read(SetSelect(lI), lRealDataKey);
      }
      std::cout << " (tot: " << read(AllSets(AllSets::TOTAL), lRealDataKey)
        << ", min/avg/max: " << read(AllSets(AllSets::MINIMUM), lRealDataKey)
        << "/" << read(AllSets(AllSets::AVERAGE), lRealDataKey)
        << "/" << read(AllSets(AllSets::MAXIMUM), lRealDataKey)
        << ")" << std::endl;
    }
  }
}

#ifdef _OLCFMEMRES_RUNSIM
int main(int aArgc, char **aArgv) {
  size_t lClock=0;
  olcfmemres::PPC440 lSim;

  // (1) open up a trace stream
  gleipnir::RecordReader lUnfilteredRR("../common/test_traces/gleipnir.1m.trace");

  // (2) exclude metadata
  gleipnir::FilteringRecordReader lRR(lUnfilteredRR, gleipnir::AccessTypes::ALL ^ gleipnir::AccessTypes::METADATA);

  olcfmemres::timer lTimer;

  // (3) read one record at a time, feed it to the simulator and deal with MODIFY
  //     type of access since this implementation does not support it.
  while (lRR.hasNext()) {
    olcfmemres::FeedQuickStatus::Value lStatus = olcfmemres::FeedQuickStatus::NONE;
    const gleipnir::Record lNextRecord(lRR.next());
    //    std::cout << lNextRecord << std::endl;
    if (lNextRecord.isDataAccess()) {
      if (lNextRecord.getAccessType() == gleipnir::AccessTypes::LOAD
          || lNextRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
        lStatus = lSim.feed(gleipnir::AccessTypes::LOAD, lNextRecord.getAddress(), 
            lNextRecord.getSize(), lClock++);
        //	if (olcfmemres::PPC440::DEBUG) {
        std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
        //	}
      }
      if (lNextRecord.getAccessType() == gleipnir::AccessTypes::STORE
          || lNextRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
        lStatus = lSim.feed(gleipnir::AccessTypes::STORE, lNextRecord.getAddress(), 
            lNextRecord.getSize(), lClock++);
        //	if (olcfmemres::PPC440::DEBUG) {
        std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
        //	}
      }
    }
  }

  // (4) dump some stats:
  lSim.dump();

  // (5) report the simulator performance:
  std::cout << std::endl << "# of accesses: " << lSim.numberOfAccesses();
  std::cout << std::endl << "rate: " << (unsigned long long)((((double) lSim.numberOfAccesses()) / ((double) lTimer.usecsElapsed()))*1000000.0) << " events/sec" << std::endl;


  return (0);
}
#endif

