// Christos: CUDA mem system simulation
#include <assert.h>
#include <algorithm>
#include <iomanip>
#include <queue>
#include <set>
#include "cuda-demultiplex.h"

namespace olcfmemres {

  class CUDA_20 : public Generic {
  public:
    static const size_t L2SIZE=768*1024;
    typedef ThreadBlockAccessDemultiplexingReader::Record Record;
    typedef ThreadBlockAccessDemultiplexingReader::WarpRecord WarpRecord;
    typedef ThreadBlockAccessDemultiplexingReader::ActiveThreadsBitSet ActiveThreadsBitSet;

    struct WarpStatistics {
      enum IntegerKey : unsigned int {
        LD_GMEM_REQUESTS=0,         ST_GMEM_REQUESTS,
          LD_GMEM_32B_TRANSACTIONS,   LD_GMEM_128B_TRANSACTIONS,
          ST_GMEM_32B_TRANSACTIONS,   ST_GMEM_128B_TRANSACTIONS,
          LD_HALF_WARP_SPLITS,        ST_HALF_WARP_SPLITS,
          // application bytes loaded or stored (excludes overhead)
          LD_GMEM_USER_VOLUME,        ST_GMEM_USER_VOLUME,
          LD_GMEM_REAL_VOLUME,        ST_GMEM_REAL_VOLUME,
          LD_GMEM_TRANSACTIONS,       ST_GMEM_TRANSACTIONS,
          LD_GMEM_UNCACHED,           ST_GMEM_UNCACHED,
          _INTEGER_KEY_COUNT
          };
      unsigned long long mIntegerKeys[_INTEGER_KEY_COUNT];
      WarpStatistics() {
        for (int lIntKey=0 ; lIntKey<(int)_INTEGER_KEY_COUNT ; lIntKey++) {
          mIntegerKeys[lIntKey] = 0;
        }
      }
      unsigned long long read(IntegerKey aKey) const { return (mIntegerKeys[(int)aKey]); }
      unsigned long long& operator[](IntegerKey aKey) { return (mIntegerKeys[(int)aKey]); }
    };


    struct WarpIdx : public Group {
      WarpIdx(size_t aWarpIdx) : mWarpIdx(aWarpIdx) {}
      size_t value() const { return (mWarpIdx); }
      size_t mWarpIdx;
    };

    WarpStatistics mStatistics[ThreadBlockAccessDemultiplexingReader::MAX_NUM_OF_WARPS];

    // Not much is known about L1/L2 eviction policies; anecdotal info talks about FIFO

    struct L2 {
      enum IntegerKey : unsigned int { 
	HIT_COUNT, MISS_COUNT, EVICTION_COUNT, _INTEGER_KEY_COUNT
      };

      L2(size_t aNumberOfLines=(768*1024/32)) 
	: mNumberOfLines(aNumberOfLines)
      {
	for (int lIntKey=0 ; lIntKey<L2::_INTEGER_KEY_COUNT ; lIntKey++) {
          mIntegerKeys[lIntKey] = 0;
        }
      }

      size_t mNumberOfLines;
      std::set<uintptr_t> mResident; // 32b-aligned addresses
      std::queue<uintptr_t> mQueue;
      unsigned long long mIntegerKeys[L2::_INTEGER_KEY_COUNT];

      bool isPresent(uintptr_t aEA) const { return (mResident.find(aEA) != mResident.end()); }
      bool establish(uintptr_t aEA, bool aDueToLoad) {
	if (isPresent(aEA)) {
	  std::cout << "l2: hit [" << (void*)aEA << ".." << (void*)(aEA+31) << "]" << std::endl;
	  mIntegerKeys[L2::HIT_COUNT]++;
	  return (false);
	}
        bool lEvicted = false;
        if (mQueue.size() == mNumberOfLines) {
          mQueue.pop();
          mResident.erase(aEA);
          lEvicted = true;
	  mIntegerKeys[L2::EVICTION_COUNT]++;
	  std::cout << "l2: evc [" << (void*)aEA << ".." << (void*)(aEA+31) << "]" << std::endl;
        }
        mQueue.push(aEA);
        mResident.insert(aEA);
	mIntegerKeys[L2::MISS_COUNT]++;
	std::cout << "l2: est [" << (void*)aEA << ".." << (void*)(aEA+31) << "]" << std::endl;
        return (lEvicted);
      }
      bool establishWide(uintptr_t aEA, bool aDueToLoad) {
	bool lAny = false;
	for (size_t lI=0 ; lI<4 ; ++lI) {
	  lAny |= establish(aEA+lI*32, aDueToLoad);
	}
	return (lAny);
      }
    };

    struct L1 {
      L1(L2& aL2, size_t aNumberOfLines, bool aIsSwitchedOn)
        : mL2(aL2),
	  mNumberOfLines(aNumberOfLines),
          mIsSwitchedOn(aIsSwitchedOn)
      { }

      L2&  mL2;
      size_t mNumberOfLines;
      bool mIsSwitchedOn;
      std::set<uintptr_t> mResident; // 128b-aligned addresses
      std::queue<uintptr_t> mQueue;

      inline bool isOn() const { return (mIsSwitchedOn); }
      bool isPresent(uintptr_t aEA) const { return (mResident.find(aEA) != mResident.end()); }
      bool establish(uintptr_t aEA, bool aDueToLoad) {
	if (isOn() && isPresent(aEA)) {
	  std::cout << "l1: hit [" << (void*)aEA << ".." << (void*)(aEA+127) << "]" << std::endl;
	  return (false);
	}

	if (isOn()) {
	  mL2.establishWide(aEA, aDueToLoad); // 4x32
	} else {
	  mL2.establish(aEA, aDueToLoad);
	}

	if (isOn()) {
	  bool lEvicted = false;
	  if (mQueue.size() == mNumberOfLines) {
	    mQueue.pop();
	    mResident.erase(aEA);
	    lEvicted = true;
	    std::cout << "l1: evc [" << (void*)aEA << ".." << (void*)(aEA+127) << "]" << std::endl;
	  }
	  mQueue.push(aEA);
	  mResident.insert(aEA);
	  std::cout << "l1: est [" << (void*)aEA << ".." << (void*)(aEA+127) << "]" << std::endl;
	  return (lEvicted);
	}
      }
    };



    L1 mL1;
    L2 mL2;

    CUDA_20() : mL1(mL2, 64, true) 
    { }
    
    void feedGmemAccess(size_t aWarpIdx, const WarpRecord& aRecord) {
      const size_t lWarpSize = ThreadBlockAccessDemultiplexingReader::WARP_SIZE;
      assert(aRecord.isActive());

      // we use this to keep track of divergent threads we have yet to process
      ActiveThreadsBitSet lActivity = aRecord.activity();
      
      while (lActivity.any()) {

	size_t lWordSize = aRecord.wordSize();
	size_t lSubWarpSize = lWarpSize / (lWordSize / 4); // xxx: what about payload<4b?
	bool lDidSomething = false;
        for (size_t lThreadOffset=0 ; lThreadOffset<lWarpSize ; lThreadOffset+=lSubWarpSize) {
	  for (size_t lThreadIdx=lThreadOffset ; lThreadIdx<(lThreadOffset+lSubWarpSize) ; ++lThreadIdx) {
	    if (lActivity.test(lThreadIdx)) {
	      uintptr_t lBase = aRecord.recordFor(lThreadIdx).getAddress();
	      size_t lTransactionSize = mL1.isOn() ? 128 : 32;
	      uintptr_t lBaseAligned = (lBase / lTransactionSize) * lTransactionSize;
	      
	      mL1.establish(lBaseAligned, aRecord.isLoad());

	      assert((mL1.isOn() && mL1.isPresent(lBaseAligned)) || mL2.isPresent(lBaseAligned));

	      // service all threads across the warp that accessed addresses hashing
	      // to the same cacheline -- these will not be re-examined again:
	      for (size_t lJ=lThreadIdx ; lJ<(lThreadOffset+lSubWarpSize) ; ++lJ) {
		if (lActivity.test(lJ)) {
		  uintptr_t lNextBaseAligned = (aRecord.recordFor(lJ).getAddress()/lTransactionSize)*lTransactionSize;
		  if (lNextBaseAligned == lBaseAligned) {
		    lActivity.reset(lJ);
		    lDidSomething = true;
		  }
		}
	      }
	    }
          }
        }

	if (!lDidSomething) {
	  std::cerr << "gpu-sim: (E) bug in warp servicing logic; activity mask: " 
		    << lActivity.to_string() << std::endl;
	  exit(-1);
	}
      }
    }

    void feedGmemAccess0(size_t aWarpIdx, const WarpRecord& aRecord) {
      assert(aRecord.isActive());
      const ActiveThreadsBitSet& lActivity = aRecord.activity();
      std::cout << "wrpact: " << lActivity.to_string() << std::endl;

      /* (1) Determine how many unique 128b segments the set of accessed addresses
       *     covers. It is important because in compute capability 2.0 & 3.0, the
       *     L1 cache may service an entire 128b line.
       */
      std::vector<uintptr_t> lAddresses;
      lAddresses.reserve(ThreadBlockAccessDemultiplexingReader::WARP_SIZE);

#define LDSTCOUNTER(X) (aRecord.isLoad() ? WarpStatistics::LD_ ## X : WarpStatistics::ST_ ## X)

      mStatistics[aWarpIdx][LDSTCOUNTER(GMEM_USER_VOLUME)] += aRecord.activity().count()*aRecord.wordSize();

     for (size_t lThreadIdx=0 ; lThreadIdx<ThreadBlockAccessDemultiplexingReader::WARP_SIZE ; ++lThreadIdx) {
	if (aRecord.isActive(lThreadIdx)) {
	  lAddresses.push_back(aRecord.recordFor(lThreadIdx).getAddress());	  
	}
      }
      std::sort(lAddresses.begin(), lAddresses.end());

      std::set<uintptr_t> lBases;
      for (const uintptr_t& lAddr : lAddresses){
	lBases.insert((lAddr / 128) * 128);
      }
      
      // TODO: we do not simulate L1 yet, so all are misses -- there will be no 128b GMEM accesses.      

      // In 2.0/3.0, # of transactions per half-warp equals the number of words in the payload
      // a 16b payload means 2 half-warps causing 4x 4b/thread transactions each (2x4x64b = 32x16b)

      size_t lNumOfRequests = aRecord.wordSize() / 4;
      mStatistics[aWarpIdx][LDSTCOUNTER(GMEM_REQUESTS)]+=lNumOfRequests;

      if (aRecord.wordSize() != 4) {
	mStatistics[aWarpIdx][LDSTCOUNTER(GMEM_UNCACHED)]++;
      }
      mStatistics[aWarpIdx][LDSTCOUNTER(GMEM_32B_TRANSACTIONS)] += lBases.size() * (aRecord.wordSize() / 4);

      for (const uintptr_t& lBase : lBases) {
	std::cout << (void*)lBase << std::endl;
      }

#undef LDSTCOUNTER
    }

    void feed(const Record& aRecord) {
      std::cout << "blcact: " << aRecord.mActiveWarps.to_string() << std::endl;
      for (size_t lWarpIdx=0 ; lWarpIdx<ThreadBlockAccessDemultiplexingReader::MAX_NUM_OF_WARPS ; ++lWarpIdx) {
	if (!aRecord.isActive(lWarpIdx)) {
	  continue;
	}
	
	const WarpRecord& lWarpRecord = aRecord.record(lWarpIdx);
	switch (lWarpRecord.memoryType()) {
        case gleipnir::SegmentTypes::CUDA_GLOBAL: { 
	  feedGmemAccess(lWarpIdx, lWarpRecord);
	  break;
	}
	default:{}
	}
      }
    }

    unsigned long long read(const WarpIdx& aWarpIdx, WarpStatistics::IntegerKey aKey) const {
      const size_t lWarpSize = ThreadBlockAccessDemultiplexingReader::WARP_SIZE;
      switch (aKey) {
      case WarpStatistics::LD_GMEM_TRANSACTIONS: { 
	return (read(aWarpIdx, WarpStatistics::LD_GMEM_32B_TRANSACTIONS) + 
		read(aWarpIdx, WarpStatistics::LD_GMEM_128B_TRANSACTIONS)); 
      }
	//      case WarpStatistics::LD_GMEM_REAL_VOLUME: {
	//	return (read(aWarpIdx, WarpStatistics::LD_GMEM_32B_TRANSACTIONS)*lWarpSize*4 +
	//                read(aWarpIdx, WarpStatistics::LD_GMEM_128B_TRANSACTIONS)*lWarpSize*);
	//      }
      default: { return (mStatistics[aWarpIdx.value()].read(aKey)); }
      };
    }
  };
}

int main(int aArgc, char **aArgv) {
  using namespace olcfmemres;
  olcfmemres::CUDA_20 lSim;

#if 0
  gleipnir::RecordReader lRR("../gpu-tracing/traces/nvidia-reduction.trace", true);

  gleipnir::CudaReader lCR("../gpu-tracing/traces/nvidia-reduction.trace");

  gleipnir::PredicateBasedFilteringRecordReader<olcfmemres::PForThread, gleipnir::CudaReader> 
    lRR_ForThread(olcfmemres::PForThread(0), lCR);
#endif

  olcfmemres::ThreadBlockAccessDemultiplexingReader lTBR(32, "test2.trace");
							 //				     "../gpu-tracing/traces/nvidia-reduction.trace");

  

  while (lTBR.hasNext())
    lSim.feed(lTBR.next());
  
  std::cout << lSim.read(CUDA_20::WarpIdx(0), CUDA_20::WarpStatistics::LD_GMEM_32B_TRANSACTIONS) << " "
	    << lSim.read(CUDA_20::WarpIdx(0), CUDA_20::WarpStatistics::LD_GMEM_USER_VOLUME) << std::endl;
    

#if 0
  while (lCR.hasNext()) {
    gleipnir::Record lRecord(lCR.next());
    //    std::cout << std::hex << lRecord.getAddress() << std::endl;
    lSim.feed(lRecord);
  }
#endif

  return (0);
}
