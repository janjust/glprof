#ifndef _OLCFMEMRES_INTERLAGOS_H
#define _OLCFMEMRES_INTERLAGOS_H

#include <limits>
#include <map>
#include "parser.h"
#include "generic.h"

namespace olcfmemres {
  class Interlagos : public Generic {
    static const bool DEBUG=false;

    static const size_t LINESIZE=64;
    static const size_t BYTE_OFFSET_ADDRESS_BITS=6; // 64b line
    static const size_t SET_ADDRESS_BITS=7;         // 128 sets, 128*2*64=16K
    static const size_t SET_ADDRESS_OFFSET=BYTE_OFFSET_ADDRESS_BITS;
    static const size_t TAG_ADDRESS_BITS=64-(BYTE_OFFSET_ADDRESS_BITS + SET_ADDRESS_BITS);
    static const size_t TAG_ADDRESS_OFFSET=BYTE_OFFSET_ADDRESS_BITS+SET_ADDRESS_BITS;
    static const size_t NWAYS=2;
    static const size_t NSETS=(16*1024)/(NWAYS*LINESIZE);

    inline size_t setFrom(uintptr_t aEA) const {
      return ((aEA >> BYTE_OFFSET_ADDRESS_BITS) % NSETS);
    }
    inline size_t addressTag(uintptr_t aEA) const {
      return (aEA >> TAG_ADDRESS_OFFSET);
    }

    struct Set {
      struct LineInfo {
	unsigned long long mAddressTag:TAG_ADDRESS_BITS;
	/** valid bit */
	unsigned  mIsSet:1;
	unsigned  mLastEventWasStore:1;
      LineInfo() : mAddressTag(0), mIsSet(0), mLastEventWasStore(0) { }
      } mArray[NWAYS];
      unsigned mMostRecentlyUsed:1;
      
      Set() : mMostRecentlyUsed(0) { }
      inline bool isFull() const { return (mArray[0].mIsSet && mArray[1].mIsSet); }
      /**
       * @return -1 if not present, otherwise the way id (0 or 1) that hodls the item
       */
      inline int lookup(uintptr_t aAddressTag) const {
	if (mArray[0].mIsSet && mArray[0].mAddressTag==aAddressTag) {
	  return (0);
	}
	if (mArray[1].mIsSet && mArray[1].mAddressTag==aAddressTag) {
	  return (1);
	}
	return (-1);
      }
      inline int makeSpaceIfNecessary() {
	if (isFull()) {
	  int lVictim = !mMostRecentlyUsed;
	  mArray[lVictim].mIsSet = 0;
	  return (lVictim);
	} else {
	  return (-1);
	}
      }
      inline bool slot0InUse() const { return (mArray[0].mIsSet); }
      inline void update(size_t aWay, uintptr_t aAddressTag) {
	mArray[aWay].mAddressTag = aAddressTag;
	mArray[aWay].mIsSet = 1;
	mMostRecentlyUsed = aWay;
      }
    } mSets[NSETS];
  public:
    Interlagos() {}
    FeedQuickStatus::Value feed(gleipnir::AccessTypes::Value aAccess, uintptr_t aEA, size_t aPayload, size_t aTimeStamp);
    static bool understands(unsigned int aEventMask);
  };    
}

#endif
