#include <iomanip>
#include <iostream>
#include <sstream>

#include "parser.h"
#include "interlagos.h"


namespace olcfmemres {
  bool Interlagos::understands(unsigned int aEventMask) {
    unsigned int lBoth = gleipnir::AccessTypes::LOAD|gleipnir::AccessTypes::STORE;
    return ((aEventMask | lBoth) == lBoth);
  }
    
  FeedQuickStatus::Value Interlagos::feed(gleipnir::AccessTypes::Value aAccess,
					  uintptr_t aEA, size_t aPayload, size_t aTimeStamp) {
    FeedQuickStatus::Value lReturnStatus = 0;
    bool lIsLoad = aAccess==gleipnir::AccessTypes::LOAD;
    size_t lSetId = setFrom(aEA);         // effective address to set
    size_t lAddressTag = addressTag(aEA); // effect address to tag

    if (DEBUG) {
      std::cout << ((void*) aEA) << " " << binary(aEA) << " " << lSetId << " " << lAddressTag << std::endl;
    }
    
    int lLookup = mSets[lSetId].lookup(lAddressTag);

    int lWay = -1;
    
    if (lLookup==-1) {
      lReturnStatus |= FeedQuickStatus::MISS;
      int lVictimized = mSets[lSetId].makeSpaceIfNecessary();
      if (lVictimized!=-1) {
	lReturnStatus |= FeedQuickStatus::EVICTION;
      }
      int lFree = mSets[lSetId].slot0InUse() ? 1 : 0;
      lWay = lVictimized!=-1 ? lVictimized : lFree;
    } else {
      lReturnStatus |= FeedQuickStatus::HIT;
      lWay = lLookup;
    }

    mSets[lSetId].update(lWay, lAddressTag);

    Set::LineInfo& lLine = mSets[lSetId].mArray[lWay];
    
    if (!lIsLoad && lLine.mLastEventWasStore) {
      lReturnStatus |= FeedQuickStatus::WAW;
    }

    lLine.mLastEventWasStore = !lIsLoad;
    
    return (lReturnStatus);    
  }
}

#ifdef _OLCFMEMRES_RUNSIM
int main(int aArgc, char **aArgv) {
  size_t lClock=0;
  olcfmemres::Interlagos lSim;

  // (1) open up a trace stream
  gleipnir::RecordReader lUnfilteredRR("../common/test_traces/gleipnir.1m.trace");

  // (2) exclude metadata
  gleipnir::FilteringRecordReader lRR(lUnfilteredRR, gleipnir::AccessTypes::ALL ^ gleipnir::AccessTypes::METADATA);

  olcfmemres::timer lTimer;

  // (3) read one record at a time, feed it to the simulator and deal with MODIFY
  //     type of access since this implementation does not support it.
  while (lRR.hasNext()) {
    olcfmemres::FeedQuickStatus::Value lStatus = olcfmemres::FeedQuickStatus::NONE;
    const gleipnir::Record lNextRecord(lRR.next());
    //    std::cout << lNextRecord << std::endl;

    if (lNextRecord.isDataAccess()) {
      if (lNextRecord.getAccessType() == gleipnir::AccessTypes::LOAD
	  || lNextRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
	lStatus = lSim.feed(gleipnir::AccessTypes::LOAD, lNextRecord.getAddress(),
			    lNextRecord.getSize(), lClock++);
	//      if (olcfmemres::PPC440::DEBUG) {
	std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
	//    }
      }
      if (lNextRecord.getAccessType() == gleipnir::AccessTypes::STORE
	  || lNextRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
	lStatus = lSim.feed(gleipnir::AccessTypes::STORE, lNextRecord.getAddress(),
			    lNextRecord.getSize(), lClock++);
	//      if (olcfmemres::PPC440::DEBUG) {
	std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
	//    }
      }
    }
  }
  
  
  return (0);
}
#endif

