#include <iomanip>
#include <sstream>
#include "statistics.h"

namespace olcfmemres {

  unsigned long long Statistics::read(Statistics::IntegerDataKey aId) const {
    switch (aId) {
    case IntegerDataKey::LOAD_COUNT: { return ((*this)[Statistics::LOAD_HIT] + (*this)[Statistics::LOAD_MISS]); }
    case IntegerDataKey::STORE_COUNT: { return ((*this)[Statistics::STORE_HIT] + (*this)[Statistics::STORE_MISS]); }
    case IntegerDataKey::ACCESS_COUNT: {
      return ((*this)[Statistics::LOAD_HIT] + (*this)[Statistics::LOAD_MISS]+
	      (*this)[Statistics::STORE_HIT] + (*this)[Statistics::STORE_MISS]);
    }
    case IntegerDataKey::TIME_UNITS_ELAPSED_SINCE_LAST_ACCESS: { return (*mMedianDistanceInTime); }
    case IntegerDataKey::SUCCESSIVE_ACCESSES_ADDRESS_DISPLACEMENT: { return (spatialLocalityTracker().distance()); }
    default: { return ((*this)[aId]); }
    }
  }

  double Statistics::read(Statistics::RealDataKey aId) const {
    switch (aId) {
    case RealDataKey::LOAD_HITMISS_RATIO : { return (((double) read(Statistics::LOAD_HIT))  / ((double) read(Statistics::LOAD_MISS)));  }
    case RealDataKey::STORE_HITMISS_RATIO: { return (((double) read(Statistics::STORE_HIT)) / ((double) read(Statistics::STORE_MISS))); }
    case RealDataKey::ACCESS_FREQUENCY: { return ( 1.0 / (double) read(IntegerDataKey::TIME_UNITS_ELAPSED_SINCE_LAST_ACCESS)); }
    default:{ return (0); }
    }
  }

  std::string FeedQuickStatus::toString(FeedQuickStatus::Value aValue) {
    std::stringstream lSS;
    std::string lAppend = "";
    if (aValue & FeedQuickStatus::HIT) {
      lSS << "HIT";
      lAppend = "|";
    } else if (aValue & FeedQuickStatus::MISS) {
      lSS << "MISS";
      lAppend = "|";
    }
    if (aValue & FeedQuickStatus::EVICTION) {
      lSS << lAppend << "EVICTION";
      lAppend = "|";
    }
    if (aValue & FeedQuickStatus::INVALIDATION) {
      lSS << lAppend << "INVALIDATION";
      lAppend = "|";
    }
    if (aValue & FeedQuickStatus::WAW) {
      lSS << lAppend << "WAW";
      lAppend = "|";
    }
    return (lSS.str());
  }
}

std::ostream& operator<<(std::ostream& aOut, const olcfmemres::Statistics& aStatistics) {
  aOut << " "
       << "[" << std::setw(5) << aStatistics.loadCount()
       << " " << std::setw(5) << aStatistics.loadHitCount()
       << " " << std::setw(5) << aStatistics.loadMissCount() << "]"
       << " "
       << "[" << std::setw(5) << aStatistics.storeCount()
       << " " << std::setw(5) << aStatistics.storeHitCount()
       << " " << std::setw(5) << aStatistics.storeMissCount() << "]"
    
       << " " << std::setw(5) << aStatistics.writeAfterWriteCount()
       << " " << std::setw(5) << aStatistics.spatialLocalityTracker().distance()
	 << " " << std::setw(5) << aStatistics.averagePeriodBetweenAccesses();
  return (aOut);
}
