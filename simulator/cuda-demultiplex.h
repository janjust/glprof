#ifndef _OLCFMEMRES_CUDA_DEMULTIPLEX_H
#define _OLCFMEMRES_CUDA_DEMULTIPLEX_H

#include <bitset>
#include <iostream>
#include "generic.h"
#include "parser.h"

namespace olcfmemres {

  struct PForThread {
  PForThread(size_t aThreadId) : mThreadId(aThreadId) {}
    inline bool operator()(const gleipnir::Record& aRecord) const {
      return (aRecord.getThreadId() == mThreadId);
    }
    size_t mThreadId;
  };

  /**
   * Demultiplexes a CUDA trace into warp records. Keep in mind that warps (a group of
   * threads, usually 32) execute independently of each other.
   *
   * x hasNext() is true if and only if at least one warp exists in an active state, which
   *   means that some warps may be inactive.
   * x next() returns ThreadBlockAccessDemultiplexingReader::Record, which holds the
   *   per-warp data. This type is not related to gleipnir::Record.
   */
  class ThreadBlockAccessDemultiplexingReader {
  public:
    static const size_t WARP_SIZE=32;
    static const size_t MAX_NUM_OF_WARPS=1024/WARP_SIZE;

    typedef std::bitset<MAX_NUM_OF_WARPS> ActiveWarpBitSet;
    typedef std::bitset<WARP_SIZE>        ActiveThreadsBitSet;

    struct EInactiveWarp {};
    struct ENoMoreRecords {};
    /** 
     * This is indication that instrumentation's divergence tracking logic, or the
     * way the tracker is used, is broken. 
     */
    struct ENonFixedSizePayload {};

    struct WarpRecord {
      ActiveThreadsBitSet mActiveThreads;
      gleipnir::Record mBufferedRecords[WARP_SIZE];
      unsigned long long mCycle;
      gleipnir::AccessTypes::Value mAccessType;
      gleipnir::SegmentTypes::Value mSegmentType;      
      size_t mPayloadSize;
  
      /**
       * Retrieves the gelipnir record corresponding to the memory access
       * issued by the given thread in this warp.
       * @param aWarpThreadId a value in the [0..WARP_SIZE) range.
       */
      const gleipnir::Record& recordFor(size_t aWarpThreadId) const {
	return (mBufferedRecords[aWarpThreadId]);
      }

      const ActiveThreadsBitSet& activity() const { return (mActiveThreads); }
      gleipnir::SegmentTypes::Value memoryType() const { return (mSegmentType); }
      bool anyDiverged() const { return (mActiveThreads!=std::bitset<WARP_SIZE>(0xFFFFFFFF)); }
      bool isActive() const { return (mActiveThreads!=std::bitset<WARP_SIZE>(0)); }
      bool isActive(size_t aWarpThreadId) const { return (mActiveThreads.test(aWarpThreadId)); }
      size_t wordSize() const { return (mPayloadSize); }
      bool isLoad() const { return (mAccessType == gleipnir::AccessTypes::LOAD); }
    };

    struct Record {
      WarpRecord                    mRecords[MAX_NUM_OF_WARPS];
      std::bitset<MAX_NUM_OF_WARPS> mActiveWarps;
      bool isActive(size_t aWarpIdx) const { return (mActiveWarps.test(aWarpIdx)); }
      const WarpRecord& record(size_t aWarpIndex) const  {
        if (!mActiveWarps.test(aWarpIndex)) {
          throw EInactiveWarp();
        }
        return (mRecords[aWarpIndex]);
      }
    };

    ThreadBlockAccessDemultiplexingReader(size_t aNumOfThreads, const char* aTrace);
    bool hasNext();
    const Record& next();
  protected:
    void advance();
    typedef gleipnir::PredicateBasedFilteringRecordReader<olcfmemres::PForThread, gleipnir::CudaReader> ThreadSpecificReader;
    size_t mNumberOfThreads;
    gleipnir::CudaReader** mCudaReaders;
    ThreadSpecificReader** mReaders;
    //    unsigned long long mPreviousCycle;
    //    unsigned long long mCycleCount;
    bool mAdvanced;
    bool mHasNext;
    Record mNext;
  };
}

#endif
