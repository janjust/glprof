#ifndef _OLCFMEMRES_GENERIC_H
#define _OLCFMEMRES_GENERIC_H

#include <map>
#include <string>
#include "parser.h"
#include "statistics.h"

namespace olcfmemres {

  /* A simulator may support event recording at different granularities:
   * per-cacheline, set-specific, page- and malloc-owner, etc.
   *
   * A simulator must implement the following method:
   *   static bool understands(unsigned int aEventMask);
   *
   * The event mask must be built by ORing AccessTypes::Value values together. One
   * may use it to test if a simulator supports a set of access types (e.g. the
   * modify may not be supported by all archs).
   *
   * The entry point to a simulator is the feed(..) method:
   *   void feed(gleipnir::AccessTypes::Value aAccess, 
   *             uintptr_t aEA, size_t aPayload, size_t aTimeStamp)
   *
   * How a simulator records events and how it computes its statistics is an
   * implementation-specific matter.
   *
   * Extracting statistics from a simulator should be modular in design since
   * we do not know in advance what kind of event aggregation or groupping a
   * new simulator or application requirements may dictate.
   *
   * Simulator implementations should provide one or more instances of the
   * following method by overloading at the type level (see Generic::read)):
   *   VALUE read(GROUP, IDENTIFIER) const;
   *
   * GROUP must not be a primitive type (int, float, etc).
   *
   * For instance, assume that we want to make available the retrieval of
   * number of accesses at both of the set & cacheline levels. First,
   * declare one new struct per group (and keep it sealed):
   *
   *   struct SetGroup  { int mSetId; ... };
   *   struct Cacheline { int mSetId, mLineNum; ... };
   *
   * Then overload read for each group and our ids enum:
   *   enum MyEvents { NUMBER_OF_ACCESSES, ... };
   *   unsigned long long read(SetGroup aSet,   MyEvents aCategory) const;
   *   unsigned long long read(Cacheline aLine, MyEvents aCategory) const;
   *
   * Categories may be declared on an as-needed basis. Certain classes,
   * however, such as Statistics, already offer some.
   */
  class Generic {
  public:

    //  template<typename VALUE_, typename GROUP_, typename ID_>
    //  VALUE_ read(GROUP_ aGroup, ID_ aCategory);

    struct Group { /* intentionaly blank, inherit only */ };

    struct MemoryRegion : public Group {
    MemoryRegion(uintptr_t aBase) : mBase(aBase) {}
      uintptr_t mBase;
    };

    typedef std::pair<uintptr_t, uintptr_t> AddressRange;

    /** per variable */
    struct Data {
      Statistics mCounters;
      inline Statistics& counters() { return (mCounters); }
      inline const Statistics& counters() const { return (mCounters); }
    };
  public:
    Data& dataFor(const AddressRange& aAR) {
      return (mMap[aAR]);
    }

    
    // TODO: range lookup by EA is currently O(N); tommy used a zig-zag tree to keep mru 
    // entries near the top of the tree..
    Data& dataFor(uintptr_t aEA) {
      for (std::map<AddressRange, Data>::iterator lI=mMap.begin() ; lI!=mMap.end() ; lI++) {
	const AddressRange& lAddr = lI->first;
	if (aEA>=lAddr.first && aEA<=lAddr.second) {
	  return (lI->second);
	}
      }
      return (mDefault);
    }
    const Data& dataFor(uintptr_t aEA) const {
      for (std::map<AddressRange, Data>::const_iterator lI=mMap.begin() ; lI!=mMap.end() ; lI++) {
	const AddressRange& lAddr = lI->first;
	if (aEA>=lAddr.first && aEA<=lAddr.second) {
	  return (lI->second);
	}
      }
      return (mDefault);
    }
    /** map an id to a range (both bounds inclusive) */
    void mapRange(const std::string& aId, uintptr_t aBegin, size_t aBytes) {
      mMap[AddressRange(aBegin, aBegin+aBytes)];
    }

    std::map<AddressRange, Data>::const_iterator rangeSpecificBegin() const { return (mMap.begin()); }
    std::map<AddressRange, Data>::const_iterator rangeSpecificEnd() const { return (mMap.end()); }

    unsigned long long read(MemoryRegion aGroup, Statistics::IntegerDataKey aId) const {
      return (dataFor(aGroup.mBase).counters().read(aId));
    }

    /**
     * Tests if the access types described in "aEventMask" are supported by this
     * simulator. This method must be implemented as a static method by the simulator
     * class that derives from this. 
     * @param aEventMask An ORed expression of AccessValues::Value items.
     */
    bool understands(unsigned int aEventMask);

    /**
     * Feeds the simulator in a new cache event (data cache at the moment).
     * @param aAccess the type of access. understands(aAccess) must hold for the access
     * but may not be tested by this method for performance reasons.
     * @param aEA the effective address
     * @param aPayload the width of the access in bytes
     * @param aTimestamp an integer indicating "when" this access occured. We
     * do not make any provisions as to what this integer represents. It must
     * always be greater than the value it had in the previous call.
     * @return A summary of the events that this access caused (see statistics.h::FeedQuickStatus)
     */
    FeedQuickStatus::Value feed(gleipnir::AccessTypes::Value aAccess, uintptr_t aEA, size_t aPayload, size_t aTimeStamp);

  protected:
    std::map<AddressRange, Data> mMap;
    Data mDefault;
  };

  std::string binary(unsigned long long v);
}


#endif
