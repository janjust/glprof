#ifndef _OLCFMEMRES_STATISTICS_H
#define _OLCFMEMRES_STATISTICS_H

#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <sys/time.h>

namespace olcfmemres {

  /**
   * Instances of Median can be used to calculate the median from a
   * stream of values. Values can be fed to an instance I via the
   * << operator (e.g. I << 100), while the current median can be
   * obtained by dereferencing the instance (*I).
   */
  struct Median {
    double mNumberOfEvents;
    double mPreviousValue;

  Median(): mNumberOfEvents(0), mPreviousValue(0) {}

    inline Median& operator <<(double aData) {
      mPreviousValue = (aData + mNumberOfEvents*mPreviousValue) / (mNumberOfEvents+1.0);
      mNumberOfEvents++;
      return (*this);
    }

    inline double operator*() const { return (mPreviousValue); }

    // median-like averge func; if the # of events for an element of the
    // input is 0 && aExcl==true, the elements is excluded.
    // TODO: not clear
    template<typename II_> static Median sum(II_ aBegin, II_ aEnd, bool aExcl=false) {
      Median lSummedUp;
      size_t lCount=0;
      for ( ; aBegin!=aEnd ; ++aBegin ) {
	const Median& lNext = *aBegin;
	lSummedUp.mPreviousValue += lNext.mPreviousValue;
	lSummedUp.mNumberOfEvents += lNext.mNumberOfEvents;
	lCount += aExcl ? (lNext.mNumberOfEvents != 0) : 1;
      }
      if (lCount!=0) {
	lSummedUp.mPreviousValue /= lCount;
      }
      return (lSummedUp);
    }
  };


  static inline uintptr_t uintptr_abs_diff(uintptr_t aX, uintptr_t aY) {
    return ((aX < aY) ? (aY-aX) : (aX-aY));
  }

  class timer {
  public:
    timer() { reset(); }
    void reset() { gettimeofday(&mBegin, NULL); }
    long long usecsElapsed() const {
      struct timeval lEnd;
      gettimeofday(&lEnd, NULL);
      return (tvToUsecs(lEnd) - tvToUsecs(mBegin));
    }
    static long long tvToUsecs(const struct timeval& aTV) {
      return (((long long) (aTV).tv_sec)*1000000LL + ((long long) (aTV).tv_usec));
    }
  protected:
    struct timeval mBegin;
  };


  struct Statistics {
    enum IntegerDataKey {
      LOAD_HIT=0, LOAD_MISS,
      STORE_HIT, STORE_MISS,
      EVICTION, INVALIDATION,
      /** write-after-write, store hit/miss followed by store hit */
      WAW,
      LOAD_COUNT, STORE_COUNT, ACCESS_COUNT,
      /** how often, in terms of cycles, an access happens */
      TIME_UNITS_ELAPSED_SINCE_LAST_ACCESS,
      /** average distance in bytes between successive accessed addresses */
      SUCCESSIVE_ACCESSES_ADDRESS_DISPLACEMENT,
      _INTEGER_KEYS_COUNT
    };

    enum RealDataKey {
      LOAD_HITMISS_RATIO=_INTEGER_KEYS_COUNT,
      STORE_HITMISS_RATIO,
      /** how many accesses per cycle -- always <=1.0 */
      ACCESS_FREQUENCY,
      _NONINTEGER_KEYS_COUNT
    };    

    static struct AvgTouched {} AVG_TOUCHED;

    static const size_t KEY_COUNT=_NONINTEGER_KEYS_COUNT;
    unsigned long long mRaw[KEY_COUNT];

    /** how many times a line is accessed before being evicted (avg) */
    Median mMedianTouched;

    /** distance, in time units, between consequtive accesses (avg) */
    Median mMedianDistanceInTime;

    /** distance, in bytes, between consequtive accesses (avg) */
    struct SpatialLocality {
      Median MedianAddrDiff;
      uintptr_t mPreviousAddress;
      bool mFirstUpdate;
    SpatialLocality() : mFirstUpdate(true) {}
      void feed(uintptr_t aEA) {
	if (!mFirstUpdate) {
	  MedianAddrDiff << uintptr_abs_diff(mPreviousAddress, aEA);
	}
	mPreviousAddress = aEA;
	mFirstUpdate = false;
      }
      size_t distance() const { return (*MedianAddrDiff); }
    } mSpatialLocalityTracker;

    /** records when last access happened */
    size_t mTimestamp;

    Statistics() {
      mTimestamp = 0;
      for (size_t lI=0 ; lI<KEY_COUNT ; lI++) mRaw[lI]=0;
    }

    unsigned long long& operator[](const IntegerDataKey& aId) { return (mRaw[aId]); }
    unsigned long long  operator[](const IntegerDataKey& aId) const { return (mRaw[aId]); }

    Median& avgTouched() { return (mMedianTouched); }
    SpatialLocality& spatialLocalityTracker() { return (mSpatialLocalityTracker); }
    const SpatialLocality& spatialLocalityTracker() const { return (mSpatialLocalityTracker); }

    void accessedAt(size_t aTimestamp) {
      if (accessesCount() > 0) {
	mMedianDistanceInTime << (aTimestamp - mTimestamp);
      }
      mTimestamp=aTimestamp;
    }

    void add(const Statistics& aStatistics) {
      for (size_t lI=0 ; lI<KEY_COUNT ; lI++) {
	mRaw[(IntegerDataKey)lI]+=aStatistics[(IntegerDataKey)lI];
      }
    }

    unsigned long long loadHitCount() const { return ((*this)[IntegerDataKey::LOAD_HIT]); }
    unsigned long long loadMissCount() const { return ((*this)[IntegerDataKey::LOAD_MISS]); }
    unsigned long long loadCount() const { return (loadHitCount() + loadMissCount()); }
    unsigned long long storeHitCount() const { return ((*this)[IntegerDataKey::STORE_HIT]); }
    unsigned long long storeMissCount() const { return ((*this)[IntegerDataKey::STORE_MISS]); }
    unsigned long long storeCount() const { return (storeHitCount() + storeMissCount()); }
    unsigned long long accessesCount() const { return (loadCount() + storeCount()); }
    unsigned long long writeAfterWriteCount() const { return ((*this)[IntegerDataKey::WAW]); }
    /** @return the average number of time units interleaving accesses */
    unsigned long long averagePeriodBetweenAccesses() const { return (*mMedianDistanceInTime); }

    double loadHitToMissRatio() const { return (((double) loadHitCount()) / ((double) loadMissCount())); }

    unsigned long long read(Statistics::IntegerDataKey aId) const;
    double read(Statistics::RealDataKey aId) const;
  };

  /**
   * The simulator collects and compiles all various of statistics.
   * Test with (feed() & FeedQuickStatus::XXX)!=0
   */
  struct FeedQuickStatus {
    typedef int Value;
    static const int NONE         = 0;
    static const int HIT          = 1;
    static const int MISS         = 2;
    static const int EVICTION     = 4;
    static const int INVALIDATION = 8;
    // set if this is a store and the last access the line experienced was a store too
    static const int WAW          = 16;
    static std::string toString(Value aValue);
  };
  
}

std::ostream& operator<<(std::ostream& aOut, const olcfmemres::Statistics& aStatistics);

#endif
