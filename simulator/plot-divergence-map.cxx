#include <iostream>
#include <sstream>
#include <iomanip>
#include "cuda-demultiplex.h"


// usage: <# OF THREADS> <TRACE-FILE>
int main(int aArgc, char **aArgv) {
  using namespace olcfmemres;
  size_t lThreadCount = atoi(aArgv[2]);
  olcfmemres::ThreadBlockAccessDemultiplexingReader lTBR(lThreadCount, aArgv[1]);

  const size_t lNumColors = 8;
  const char* lPalette[lNumColors] = {
    "#AAAAAA", "#0000FF", "#000088", "#FF0000", "#880000", "#00ff00", "#008800", "#555555"
  };
  const char* lLegend[lNumColors] = {
    "not in group", "gmem load", "gmem store", "smem load", "smem store", "cmem load", "cmem store", "tmem load"
  };

  std::cout << "reset" << std::endl;
  std::cout << "set size 1,1" << std::endl
	    << "set multiplot layout 2,1" << std::endl
	    << "set origin 0.95,0" << std::endl
	    << "set size   0.05,1" << std::endl;

  for (size_t lI=0 ; lI<lNumColors ; lI++) {
    std::cout << "set style line " << (lI+1) 
	      << " lc rgb '" << lPalette[lI] << "' pt 5" << std::endl;
  }
  std::cout << "unset border; unset xtics; unset ytics; set xrange[-1:0]; set yrange[-1:0]" << std::endl;
  const char* lSeparator = " ";
  std::cout << "plot";
  for (size_t lI=0 ; lI<lNumColors ; lI++) {
    std::cout << lSeparator << "'-' w p ls " << (lI+1)
	      << " title '" << lLegend[lI] << "'";
    lSeparator = ", ";
  }
  std::cout << std::endl;
  for (size_t lI=0 ; lI<lNumColors ; lI++) {
    std::cout << "1 2" << std::endl << "e" << std::endl;
  }

  std::cout << "set origin 0.0,0" << std::endl 
	    << "set size 0.95,1" << std::endl
	    << "set border; set xtics; set ytics"  << std::endl;


  std::stringstream lData;
  size_t lCycles=0;
  const size_t lWarpSize = ThreadBlockAccessDemultiplexingReader::WARP_SIZE;
  size_t lCycleCount=0;
  while (lTBR.hasNext()) {
    const ThreadBlockAccessDemultiplexingReader::Record& lRecord = lTBR.next();
    for (size_t lWI=0 ; lWI<(lThreadCount/lWarpSize) ; ++lWI) {
      if (lRecord.isActive(lWI)) {
	const ThreadBlockAccessDemultiplexingReader::WarpRecord& lWRecord = lRecord.record(lWI);
	for (size_t lTI=0 ; lTI<lWarpSize ; ++lTI) {
	  int lValue = 0;
	  if (lWRecord.isActive(lTI)) {
	    switch (lWRecord.memoryType()) {
	    case  gleipnir::SegmentTypes::CUDA_GLOBAL: { lValue=0; break; }
	    case  gleipnir::SegmentTypes::CUDA_SHARED: { lValue=1; break; }
	    }
	    lValue = (lValue*2 + !lWRecord.isLoad()) + 1;
	  }
	  lData << lValue << " ";
	}      
      } else {
	for (size_t lTI=0 ; lTI<lWarpSize ; ++lTI) {
	  lData << 0 << " ";
	}
      }
    } 
    lCycles++;
    lData << std::endl;
  }
  lData << "e" << std::endl;

  std::cout << "set xrange[-0.5:" << (lThreadCount-1) << ".5]" << std::endl
	    << "set yrange[-0.5:" << (lCycles-1)      << ".5]" << std::endl;
  

  std::cout << "set size ratio 1" << std::endl
    //            << "set palette gray negative" << std::endl
            << "set yrange[] reverse" << std::endl
	    << "set cbrange[0:" << (lNumColors-1) << "]" << std::endl
	    << "set palette model RGB maxcolors " << lNumColors << std::endl;

  std::cout << "set palette model RGB defined (";
  lSeparator = "";
  for (size_t lI=0 ; lI<lNumColors ; lI++) {
    std::cout << lSeparator << lI << " '" << lPalette[lI] << "'";
    lSeparator = ", ";
  }
  std::cout << ")" << std::endl;

  std::cout << "unset colorbox" << std::endl
            << "set xtics 0,1" << std::endl
            << "set ytics 0,1" << std::endl
            << "set ylabel \"Convergence Cycle\"" << std::endl
            << "set xlabel \"Thread Id\"" << std::endl
            << "set title \"Divergence Map\"" << std::endl
            << "set tics scale 0,0.001" << std::endl
            << "set mxtics 2" << std::endl
            << "set mytics 2" << std::endl
            << "set grid front mxtics mytics lw 1.5 lt -1 lc rgb 'white'" << std::endl
            << "plot \"-\" matrix w image noti" <<std::endl
	    << lData.str();


}
