#ifndef _OLCFMEMRES_PPC440_H
#define _OLCFMEMRES_PPC440_H

#include <limits>
#include <map>
#include "parser.h"
#include "generic.h"

namespace olcfmemres {
  /**
   * PPC440: first 5 bits is offset, next 4 is set, rest is address tag
   * The "address tag" is the the effective address with the set & line offset bits cleared.
   *
   * NOTE: this implementation only understands LOADs & STOREs.
   */
  class PPC440 : public Generic {
  public:
    static const bool DEBUG = false;
  private:
    static const size_t NSETS=16;
    static const size_t NWAYS=64;
    static const size_t BYTE_OFFSET_ADDRESS_BITS=5;
    static const size_t LINESIZE=1 >> BYTE_OFFSET_ADDRESS_BITS;
    static const size_t SET_ADDRESS_BITS=4;
    static const size_t SET_ADDRESS_OFFSET=BYTE_OFFSET_ADDRESS_BITS+SET_ADDRESS_BITS;
    static const size_t TAG_ADDRESS_BITS=64-(BYTE_OFFSET_ADDRESS_BITS + SET_ADDRESS_BITS);
    static const size_t TAG_ADDRESS_OFFSET=SET_ADDRESS_OFFSET+TAG_ADDRESS_BITS-1;

    struct Set {
      struct LineInfo {
	unsigned long long mAddressTag:TAG_ADDRESS_BITS;
	/** valid bit */
	unsigned  mIsSet:1;
	unsigned  mLastEventWasStore:1;
	/** times this line has been touched since it became valid */
	size_t mTouched;
        LineInfo() : mAddressTag(0), mIsSet(0), mLastEventWasStore(0), mTouched(0) { }
      } mArray[NWAYS];

      /** maps (currently valid) address tags to ways (i.e. mArray above)
       *  TODO: the LineInfo::mAddressTag is then kinda redundant
       */
      std::map<uintptr_t, size_t> mAddressTagToWay;

      /** on a miss, we will always write to the way following this one (victim) */
      size_t mLastWayUsed;

      Statistics mCounters;
      unsigned long long mNumberOfAccesses;
      Median mMedianTouched;

      /** distance between accessed addresses falling in this set */
      Median mMedianAddrDiff;

      uintptr_t mPreviousAddress;
      Set() : mLastWayUsed(NWAYS-1), mNumberOfAccesses(0) { }
    };

    Set mSets[NSETS];
    size_t mNumberOfAccesses;

    inline size_t setFrom(uintptr_t aEA) const {
      return ((aEA >> BYTE_OFFSET_ADDRESS_BITS) % (1<<SET_ADDRESS_BITS));
    }
    inline size_t addressTag(uintptr_t aEA) const {
      return (aEA >> SET_ADDRESS_OFFSET);
    }
  public:
    /**
     * AllSets can be used to obtain statistics from all sets, via the
     * read interface. E.g.: read(AllSets(AllSets::AVERAGE), Statistics::STORE_MISS), to
     * get the avergae number of store misses across the entire cache.
     */
    struct AllSets : public Group { 
      enum Value { TOTAL, MINIMUM, MAXIMUM, AVERAGE } mMode; 
      AllSets(Value aMode) : mMode(aMode) {}
      Value mode() const { return (mMode); }
    };

    /**
     * SetSelect can be used to obtain statistics from a specific set, via the
     * read interface. E.g.: read(SetSelect(2), Statistics::STORE_MISS), to
     * get the average number of store misses across the entire cache. The set
     * index is 0-based.
     */
    struct SetSelect : public Group {
      SetSelect(int aSetIndex) : mSetIndex(aSetIndex) {}
      inline int index() const { return (mSetIndex); }
      int mSetIndex;
    };


    PPC440() : mNumberOfAccesses(0) {}
    size_t numberOfAccesses() const { return (mNumberOfAccesses); }
    void dump();

    static bool understands(unsigned int aEventMask);
    
    FeedQuickStatus::Value feed(gleipnir::AccessTypes::Value aAccess, uintptr_t aEA, size_t aPayload, size_t aTimeStamp);

    double read(const SetSelect& aSet, Statistics::RealDataKey aId) const;    
    unsigned long long read(const SetSelect& aSet, Statistics::IntegerDataKey aId) const;

    unsigned long long read(const AllSets& aAllSets, Statistics::IntegerDataKey aId) const;
    double read(const AllSets& aAllSets, Statistics::RealDataKey aId) const;

  private:    
    template<typename K_, typename V_> V_ read0(const AllSets& aAllSets, K_ aId) const {
      V_ lValueMin=read(SetSelect(0), aId);
      V_ lValueMax = lValueMin, lTotal = lValueMin;

      for (size_t lSetIndex=1 ; lSetIndex<NSETS ; lSetIndex++) {
	V_ lValue = read(SetSelect(lSetIndex), aId);
	lValueMin = std::min(lValueMin, lValue);
	lValueMax = std::max(lValueMax, lValue);
	lTotal += lValue;
      }
      switch (aAllSets.mode()) {
      case AllSets::MINIMUM: { return (lValueMin); }
      case AllSets::MAXIMUM:  {return (lValueMax); }
      case AllSets::TOTAL: { return (lTotal); }
      case AllSets::AVERAGE: { return (lTotal / NSETS); }
      default:{ return ((V_)-1); }
      }
    }

  };
}

#endif
