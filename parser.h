#ifndef _PARSER_H
#define _PARSER_H

#include <string>
#include <set>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>

namespace gleipnir {

  class EMalformed {};

  struct AccessTypes {
    enum Value : unsigned int {
      UNKNOWN=0, INSTRUCTION=1, LOAD=2, STORE=4, MODIFY=8, METADATA=16
    };
    static const unsigned int ALL = INSTRUCTION|LOAD|STORE|MODIFY|METADATA;
  };

  struct SegmentTypes {
    enum Value {
      UNKNOWN=0,
      // CPU-specific
      STACK=1, GLOBAL=2, HEAP=3,
      // CUDA-specific
      CUDA_GLOBAL=1, CUDA_LOCAL=2, CUDA_SHARED=3, CUDA_CONSTANT=4, CUDA_TEXTURE=5
    };
  };

  struct AccessScopes {
    enum Value {
      UNKNOWN,
      LOCAL_STRUCTURE, LOCAL_VARIABLE,
      GLOBAL_STRUCTURE, GLOBAL_VARIABLE,
      HEAP_ALLOC
    };
  };

  struct Metadata {
    enum Value {
      UNKNOWN=0,
      START, THREAD_CREATE, FORK, GL_UMSG_STR, UPDATE_MSTRUCT, MARK, MALLOC,
    };
  };

  class Record {
    public:
      struct ENotOwnedByAFunction {};

      explicit Record(unsigned long long aInstructionCount=0, 
          Metadata::Value aId=Metadata::UNKNOWN, 
          const std::string* aObjectName=NULL, 
          size_t aSize=0, uintptr_t aAddress=0);

      /** Instruction-only constructor */
      explicit Record(uintptr_t aAddress, const std::string* aOwnerFunction, 
          size_t aSize, int aThreadId);

      /** Generic constructor */
      explicit Record(uintptr_t aAddress, const std::string* aOwnerFunction, 
          size_t aSize, int aThreadId,
          AccessTypes::Value aAccessType, SegmentTypes::Value aSegmentType,
          const std::pair<AccessScopes::Value,int>& aAccessScope,
          const std::string* aVarName=NULL,
          unsigned long long aVarValue=0,
          unsigned long long aCycle=0);

      /** NOTE: this is legacy; obtain records either through the readers, or
       *  the RecordParser, as this is more efficient.
       */
      static Record obtain(const std::string& aLine,
          std::set<std::string>& aAllStringsEncountered);

      inline const std::string&  ownerFunction() const { 
        if (isMetadata()) {
          throw (ENotOwnedByAFunction());
        } 
        return (*mOwnerFunction); 
      }

      inline const std::string& getVarName() const {
        if (isMetadata()) {
          throw (ENotOwnedByAFunction());
        }

        return (*mVarName);
      }

      inline AccessTypes::Value  getAccessType() const { return (mAccessType); }
      inline SegmentTypes::Value getSegmentType() const { return (mUnion0.mSegmentType); }              
      inline uintptr_t           getAddress() const { return(mAddress); }
      inline size_t              getSize() const { return (mSize); }

      bool                       isMetadata() const { return (getAccessType() == AccessTypes::METADATA); }
      bool                       isDataAccess() const {
        return (getAccessType() & (AccessTypes::LOAD|AccessTypes::STORE|AccessTypes::MODIFY));
      }
      bool                       hasFunction() const {
        return ( *mOwnerFunction == "" ? false : true );
      }
      bool                       hasVariable() const {
        if(mVarName != NULL){
          return ( *mVarName == "" ? false : true );
        }
        else{
          return false;
        }
      }
      inline size_t              getThreadId() const { return (mThreadId); }
      inline unsigned long long  getCycle() const { return (mCycle); }
      inline bool                isLoad(bool aStrictly=true) const { 
        return (getAccessType()==AccessTypes::LOAD || (!aStrictly && getAccessType()==AccessTypes::MODIFY)); 
      }

    protected:
      AccessTypes::Value                 mAccessType;
      union {
        SegmentTypes::Value              mSegmentType;
        Metadata::Value                  mMetadataId;
      }                                  mUnion0;
      intptr_t                           mAddress;
      size_t                             mSize;
      int                                mThreadId;
      std::pair<AccessScopes::Value,int> mAccessScope;
      const std::string*                 mOwnerFunction;
      const std::string*                 mVarName;
      unsigned long long                 mVarValue;
      unsigned long long                 mCycle;
  };

  class RecordParser {
    public:
      RecordParser(bool aIsCudaStream=false) : mIsCudaStream(aIsCudaStream) {}
      Record obtain(const std::string& aLine, std::set<std::string>& aAllStringsEncountered);
    protected:
      bool              mIsCudaStream;
      std::stringstream mSS;
      std::string       mTmp;
  };

  /**
   * Usage:
   *  RecordReader lRR("common/gleipnir.100k.trace");
   *  while (lRR.hasNext()) { Record lRec = lRR.next(); }
   */
  class RecordReader : private RecordParser {
    public:
      struct EFailed {};

      RecordReader(bool aIsCudaStream=false) : RecordParser(aIsCudaStream), mHasNext(false) { }
      RecordReader(const char* aPath, bool aIsCudaStream=false);

      inline bool hasNext() const { return (mHasNext); }

      /**
       * Returns the next record. Records will stay valid as long as this instance remains valid. 
       */
      Record next();

      std::set<std::string>& stringsSet() { return (mAllStringsEncountered); }
    protected:
      bool mHasNext;
      std::ifstream mInput;
      std::string mLine;
      std::set<std::string> mAllStringsEncountered;

      void advance();
  };

  class CudaReader : public RecordReader {
    public:
      CudaReader(const char* aPath) : RecordReader(aPath, true) { }
  };

  /**
   * A PredicateBasedFilteringRecordReader provides the same interface as the RecordReader but ommits
   * specific records. The offensive records will be simply skipped. The reader operates
   * in terms of the hasNext()/next() protocol -- if hasNext() is true, next() will succeed. In
   * addition, this implementation also offers a peek() method.
   *
   * Template argument PFUNC_ must implement the following method:
   *   bool operator()(const Record&) const;
   *
   * The purpose of this class is to use a functor object to select/discard records coming
   * out of a reader. Only those records which for the predicate holds for will be
   * returned.
   */
  template<typename PFUNC_, typename RECORD_READER_=RecordReader>
    class PredicateBasedFilteringRecordReader {
      public:
        /*
         * @param aPredFunc The predicate functor, which will be copied by this constructor.
         * @param aRR A records reader. The object should be available throughout for the
         * entire lifetime of this instance.
         */
        PredicateBasedFilteringRecordReader(const PFUNC_& aPredFunc, RECORD_READER_& aRR) 
          : mPredFunc(aPredFunc), mRecordReader(aRR) 
        { }
        const Record& next() {
          if (!hasNext()) throw(RecordReader::EFailed());
          mChecked = false;
          return (mBuffered);
        }
        const Record& peek() {
          if (!hasNext()) throw(RecordReader::EFailed());
          return (mBuffered);
        }
        inline bool hasNext() {
          ready();
          return (mHasNext);
        }
        inline void ready() { if (!mChecked) advance(); }
      protected:
        void advance() {
          mHasNext = false;
          mChecked = true;
          while (mRecordReader.hasNext()) {
            mBuffered = mRecordReader.next();
            if (mPredFunc(mBuffered)) {
              mHasNext = true;
              return;
            }
          }
        }

        const PFUNC_ mPredFunc;
        RECORD_READER_& mRecordReader;
        bool mChecked;
        bool mHasNext;
        Record mBuffered;
    };

  /**
   * A FilteringRecordReader provides the same interface as the RecordReader but ommits
   * specific records. The offensive records will be simply skipped. The reader operates
   * in terms of the hasNext()/next() protocol.
   *
   * Usage (all records appart X-mode) is similar to RecordReader:
   *   // obtain a reader for a given file:
   *   RecordReader lUnfilteredRR("common/gleipnir.100k.trace");
   *   // set up a filtering reader that sources records from the reader above:
   *   FilteringRecordReader lRR(lUnfilteredRR, gleipnir::AccessTypes::ALL ^ gleipnir::AccessTypes::METADATA);
   */
  class FilteringRecordReader {
    public:
      /**
       * @param aRR an existing records reader which this instance will use to obtain records from.
       * @param aAccessTypesMask signifies which access type(s) records must have and is obtained
       *        by ORing members from the gleipnir::AccessTypes together. If a record's access type
       *        does not match, the record will not be returned by next().
       *        Example: gleipnir::AccessTypes::LOAD|gleipnir::AccessTypes::STORE for load/store
       *        records only.
       */
      FilteringRecordReader(RecordReader& aRR, unsigned int aAccessTypesMask) 
        : mRecordReader(aRR), mAccessTypesMask(aAccessTypesMask), mChecked(false), mHasNext(false) 
      { }

      inline bool hasNext() {
        if (!mChecked)
          advance();

        return (mHasNext);
      }

      Record next();

    protected:
      void advance();
      RecordReader& mRecordReader;
      unsigned int mAccessTypesMask;
      bool mChecked;
      bool mHasNext;
      Record mBuffered;
  };

  /**
   * A map from 0-indexed, increasing, integer keys to C strings.
   * The table can be initialized in two ways: 
   *   (1) by adding strings to it and calling seal() once done, or 
   *   (2) by passing it the memory address and length of the buffer that holds 
   *       a "serialized" form of this table.
   * The first method is preferred when a new table is being built.
   * A serialized version can be obtained by storing the contents of the
   * buffer starting at base(), which will be sizeInBytes() bytes long.
   */
  class StringsTable {
    public:
      struct ENotFound {};
      struct ESealed {};
      struct ENotSealed {};

      StringsTable();

      /**
       * Constructs a table directly from raw data. "aLength" must be the length, in
       * bytes, of "aBegin". "aBegin" should remain valid and unmodified for the entire
       * duration of this instance's lifetime.
       */
      StringsTable(const char* aBegin, size_t aLength);

      StringsTable(const char* aFilePath);

      ~StringsTable();

      /**
       * Insert the string into the table (if not already there) and returns
       * a key that uniquely identifies the key. seal() must be called once
       * insertions have been completed.
       */
      size_t insert(const std::string& aString);   

      void seal();

      const char* base() const;

      /** @return the number of entries in this table */
      size_t size() const { return (mIndexToOffset.size()); }

      size_t sizeInBytes() const { return (mSize); }

      /**
       * Retrieves the string by key. 
       */
      const char* operator[](size_t aKey) const;
      void dump() const;
    protected:
      std::pair<char*,size_t> mStrings;
      std::map<std::string, size_t> mStringToIndex;
      std::map<size_t,size_t> mIndexToOffset;
      size_t mSize;
      size_t mNextOffset;
      bool mSealed;
  };
}

std::ostream& operator<<(std::ostream& os, const gleipnir::Record& rd);

extern "C" void* gleipnir_stringstable_create();
extern "C" int   gleipnir_stringstable_put(void *aTable, const char* aString);
extern "C" void  gleipnir_stringstable_store_and_destroy(void *aTable, FILE* aOut);

#endif
