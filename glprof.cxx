#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string.h>
#include <set>
#include <stack>
#include <map>
#include <vector>

#include "parser.h"
#include "ppc440.h"

struct cache_t {
  unsigned long hits;
  unsigned long miss;

  /* I forgot the reasoning for these, probably the same as for Map values */
  unsigned long thits;
  unsigned long tmiss;

  cache_t() {
    hits = 0;
    miss = 0;
    thits = 0;
    tmiss =0;
  }
};

struct counter{
  unsigned long int load,
                store,
                modify,
                instruction,
                metadata;

  /* I need these for the Map values - otherwise I have to create a new map */
  unsigned long int tload,
                tstore,
                tmodify,
                tinstruction,
                tmetadata;

  cache_t ppc440;

  std::map<std::string, counter> cMap;
  std::map<std::string, counter> vars;
  std::set<std::string> gParents;

  /* constructor */
  counter() {
    load = 0;
    store = 0;
    modify = 0;
    instruction = 0;
    metadata = 0;

    tload = 0;
    tstore = 0;
    tmodify = 0;
    tinstruction = 0;
    tmetadata = 0;

    ppc440.hits = 0;
    ppc440.miss = 0;
    ppc440.thits = 0;
    ppc440.tmiss = 0;
  }

  /* overloaded operator */
  unsigned long & operator [] (gleipnir::AccessTypes::Value v) {
    if(v == gleipnir::AccessTypes::LOAD)
      return load;
    else if (v == gleipnir::AccessTypes::STORE)
      return store;
    else if (v == gleipnir::AccessTypes::MODIFY)
      return modify;
    else if (v == gleipnir::AccessTypes::INSTRUCTION)
      return instruction;
    else if (v == gleipnir::AccessTypes::METADATA)
      return metadata;
    else
      throw v;
  }
};

struct stack_counter{
  std::string   fnname;
  unsigned long addr;

  counter METRIC;  // mine
  counter tMETRIC; // total
  std::set<std::string> gParents;

  /* constructor */
  stack_counter() {
    fnname[0] = '\0';
    addr = 0;
  }
};


int main(int aArgc, char **aArgv)
{

  /* simulator ppc440 */
  size_t lClock = 0;
  olcfmemres::PPC440 lSim;

  /* read line, filter METADATA */
  gleipnir::RecordReader lRR0(aArgv[1]);
  gleipnir::FilteringRecordReader lRR(lRR0, gleipnir::AccessTypes::ALL ^ gleipnir::AccessTypes::METADATA);

  /* map of functions to track */
  std::map<std::string, counter> myMap;
  std::map<std::string, counter> tmpMap;

  std::map<std::string, counter>::iterator it;
  std::map<std::string, counter>::iterator sit;

  /* stack */
  std::stack<stack_counter> mainStack;
  stack_counter             stackbuf;

  counter myCounter;

//  /* Print Stack-Trace */
//  std::cout << std::right;
//  std::cout << std::setw(9) << "I"
//    << std::setw(9) << "L"
//    << std::setw(9) << "S"
//    << std::setw(9) << "M";
//  std::cout << std::left << "  START\n";

  /* read trace */
  long unsigned itteration = 0;
  while ( lRR.hasNext() ) {
    gleipnir::Record myRecord ( lRR.next() );

    olcfmemres::FeedQuickStatus::Value lStatus = 0;

    /* aType Addr Size tId Size - standard trace line */
    gleipnir::AccessTypes::Value  aType    = myRecord.getAccessType();
    uintptr_t                     addr     = myRecord.getAddress();
    gleipnir::SegmentTypes::Value segType  = myRecord.getSegmentType();
    std::string varname                    = "";

    if (myRecord.isDataAccess()) {
      if (myRecord.getAccessType() == gleipnir::AccessTypes::LOAD
          || myRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
        lStatus = lSim.feed(gleipnir::AccessTypes::LOAD, myRecord.getAddress(),
            myRecord.getSize(), lClock++);
        //	if (olcfmemres::PPC440::DEBUG) {
        //std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
        //	}
      }
      if (myRecord.getAccessType() == gleipnir::AccessTypes::STORE
          || myRecord.getAccessType() == gleipnir::AccessTypes::MODIFY) {
        lStatus = lSim.feed(gleipnir::AccessTypes::STORE, myRecord.getAddress(),
            myRecord.getSize(), lClock++);
        //	if (olcfmemres::PPC440::DEBUG) {
        //std::cout << olcfmemres::FeedQuickStatus::toString(lStatus) << std::endl;
        //	}
      }
    }

    unsigned int hits = 0;
    unsigned int miss = 0;

    if((lStatus & olcfmemres::FeedQuickStatus::MISS) != 0){
      miss = 1;
    }
    else if((lStatus & olcfmemres::FeedQuickStatus::HIT) != 0){
      hits = 1;
    }


    /* function name - optional though usually present */
    if( !myRecord.hasFunction() ){
      continue; /* ignore this line */
    }

    const std::string& fnname   = myRecord.ownerFunction();

    /* variable name - optional present only for H and G segments
     * - ignore S vars */
    if( myRecord.hasVariable() ){
      varname = myRecord.getVarName();
    }

    /* update stack */
    if( mainStack.size() == 0){
      stack_counter newStack;

      /* first instance on stack */
      newStack.fnname = fnname;
      newStack.addr   = addr;
      newStack.METRIC[aType]  = 1;
      newStack.tMETRIC[aType] = 1;

      /* add cache values */
      newStack.METRIC.ppc440.hits  += hits;
      newStack.METRIC.ppc440.hits  += miss;
      newStack.tMETRIC.ppc440.hits += hits;
      newStack.tMETRIC.ppc440.miss += miss;

      if(varname != ""){
        newStack.METRIC.vars[varname][aType]++;
        newStack.tMETRIC.vars[varname][aType]++;

        /* add cache values */
        newStack.METRIC.vars[varname].ppc440.hits += hits;
        newStack.METRIC.vars[varname].ppc440.miss += miss;

        newStack.tMETRIC.vars[varname].ppc440.hits += hits;
        newStack.tMETRIC.vars[varname].ppc440.miss += miss;
      }

      mainStack.push(newStack);

//      /* Print Stack-Trace */
//      std::cout << std::setw((4*9) + 2) << " ";
//      std::cout << std::setw( mainStack.size() * 2 ) << " " << "-> " << mainStack.top().fnname << std::endl;
    }
    else{
      /* new function or same function ? */
      if(fnname != mainStack.top().fnname){
        stack_counter newStack;

        /* new function */

        /* Some functions start their call with
         * an access to a G or H type.
         * It's tricky to determine the call type based on G and H types, so only do for S types.
         */

        /* is this ret or call? */
        if(segType == gleipnir::SegmentTypes::STACK){
          if(mainStack.top().addr > addr){
            /* call */
            newStack.fnname = fnname;
            newStack.addr   = addr;
            newStack.METRIC[aType]++;
            newStack.tMETRIC[aType]++;

            /* add cache values */
            newStack.METRIC.ppc440.hits += hits;
            newStack.METRIC.ppc440.miss += miss;
            newStack.tMETRIC.ppc440.hits += hits;
            newStack.tMETRIC.ppc440.miss += miss;

            if(varname != ""){
              newStack.METRIC.vars[varname][aType]++;
              newStack.tMETRIC.vars[varname][aType]++;

              /* add cache values */
              newStack.METRIC.vars[varname].ppc440.hits += hits;
              newStack.METRIC.vars[varname].ppc440.miss += miss;

              newStack.tMETRIC.vars[varname].ppc440.hits += hits;
              newStack.tMETRIC.vars[varname].ppc440.miss += miss;
            }

            newStack.gParents.insert(mainStack.top().fnname);

            mainStack.push(newStack);

//            /* Print Stack-Trace */
//            std::cout << std::setw((4*9) + 2) << " ";
//            std::cout << std::setw( mainStack.size() * 2 ) << " " << "-> " << fnname << std::endl;
          }
          else{
            /* ret */
            stackbuf = mainStack.top();

//            /* Print Stack-Trace */
//            std::cout << std::right
//              << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::INSTRUCTION]
//              << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::LOAD]
//              << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::STORE]
//              << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::MODIFY]
//              << std::setw(2) << " "
//              << std::setw( mainStack.size() * 2 ) << " ";
//            std::cout << "<- " << stackbuf.fnname << std::endl;

            mainStack.pop();

            /* update_matrix(self) / .structures() / .gParents() */
            myMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION]  +=stackbuf.METRIC.instruction;
            myMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]         +=stackbuf.METRIC.load;
            myMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]        +=stackbuf.METRIC.store;
            myMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]       +=stackbuf.METRIC.modify;

            myMap[stackbuf.fnname].ppc440.hits   += stackbuf.METRIC.ppc440.hits;
            myMap[stackbuf.fnname].ppc440.miss   += stackbuf.METRIC.ppc440.miss;

            myMap[stackbuf.fnname].tinstruction +=stackbuf.tMETRIC.instruction;
            myMap[stackbuf.fnname].tload        +=stackbuf.tMETRIC.load;
            myMap[stackbuf.fnname].tstore       +=stackbuf.tMETRIC.store;
            myMap[stackbuf.fnname].tmodify      +=stackbuf.tMETRIC.modify;

            myMap[stackbuf.fnname].ppc440.thits  += stackbuf.tMETRIC.ppc440.hits;
            myMap[stackbuf.fnname].ppc440.tmiss  += stackbuf.tMETRIC.ppc440.miss;

            /* update variables/structures */
            for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
              myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::INSTRUCTION]  += (sit->second).instruction;
              myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::LOAD]         += (sit->second).load;
              myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::STORE]        += (sit->second).store;
              myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::MODIFY]       += (sit->second).modify;

              /* update cache values */
              myMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
              myMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;
            }
            for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
              myMap[stackbuf.fnname].vars[(sit->first)].tinstruction += (sit->second).instruction;
              myMap[stackbuf.fnname].vars[(sit->first)].tload        += (sit->second).load;
              myMap[stackbuf.fnname].vars[(sit->first)].tstore       += (sit->second).store;
              myMap[stackbuf.fnname].vars[(sit->first)].tmodify      += (sit->second).modify;

              /* update cache values */
              myMap[stackbuf.fnname].vars[(sit->first)].ppc440.thits += (sit->second).ppc440.hits;
              myMap[stackbuf.fnname].vars[(sit->first)].ppc440.tmiss += (sit->second).ppc440.miss;
            }

            std::set<std::string>::iterator git;
            for(git = stackbuf.gParents.begin(); git != stackbuf.gParents.end(); ++git){
              myMap[stackbuf.fnname].gParents.insert(*git);
            }

            /* if stack is empty, continue */
            if( mainStack.size() == 0 ){
              continue;
            }

            /* update totals stack(parent)  */
            mainStack.top().tMETRIC.instruction += stackbuf.tMETRIC.instruction;
            mainStack.top().tMETRIC.load        += stackbuf.tMETRIC.load;
            mainStack.top().tMETRIC.store       += stackbuf.tMETRIC.store;
            mainStack.top().tMETRIC.modify      += stackbuf.tMETRIC.modify;

            mainStack.top().tMETRIC.ppc440.hits += stackbuf.tMETRIC.ppc440.hits;
            mainStack.top().tMETRIC.ppc440.miss += stackbuf.tMETRIC.ppc440.miss;

            /* update stack(parent).variables/structures */
            for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
              mainStack.top().tMETRIC.vars[(sit->first)].instruction += (sit->second).instruction;
              mainStack.top().tMETRIC.vars[(sit->first)].load += (sit->second).load;
              mainStack.top().tMETRIC.vars[(sit->first)].store += (sit->second).store;
              mainStack.top().tMETRIC.vars[(sit->first)].modify += (sit->second).modify;

              mainStack.top().tMETRIC.vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
              mainStack.top().tMETRIC.vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;
            }

            /* update matrix(parent)->self) */
            it = myMap.find(mainStack.top().fnname);
            if(it == myMap.end()){
              /* not found */
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION]
                += stackbuf.METRIC.instruction;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]
                += stackbuf.METRIC.load;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]
                += stackbuf.METRIC.store;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]
                += stackbuf.METRIC.modify;

              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tinstruction += stackbuf.tMETRIC.instruction;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tload        += stackbuf.tMETRIC.load;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tstore       += stackbuf.tMETRIC.store;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tmodify      += stackbuf.tMETRIC.modify;

              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.thits += stackbuf.tMETRIC.ppc440.hits;
              myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.tmiss += stackbuf.tMETRIC.ppc440.miss;

              /* update_matrix(parent.self).structures */
              for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].instruction += (sit->second).instruction;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].load        += (sit->second).load;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].store       += (sit->second).store;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].modify      += (sit->second).modify;

                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.hits += (sit->second).ppc440.hits;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.miss += (sit->second).ppc440.miss;
              }
              for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tinstruction += (sit->second).instruction;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tload        += (sit->second).load;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tstore       += (sit->second).store;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tmodify      += (sit->second).modify;

                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.thits += (sit->second).ppc440.hits;
                myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.tmiss += (sit->second).ppc440.miss;
              }
            }
            else{
              /* found */
              (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION]  += stackbuf.METRIC.instruction;
              (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]         += stackbuf.METRIC.load;
              (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]        += stackbuf.METRIC.store;
              (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]       += stackbuf.METRIC.modify;

              (it->second).cMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
              (it->second).cMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

              (it->second).cMap[stackbuf.fnname].tinstruction += stackbuf.tMETRIC.instruction;
              (it->second).cMap[stackbuf.fnname].tload        += stackbuf.tMETRIC.load;
              (it->second).cMap[stackbuf.fnname].tstore       += stackbuf.tMETRIC.store;
              (it->second).cMap[stackbuf.fnname].tmodify      += stackbuf.tMETRIC.modify;

              (it->second).cMap[stackbuf.fnname].ppc440.thits += stackbuf.tMETRIC.ppc440.hits;
              (it->second).cMap[stackbuf.fnname].ppc440.tmiss += stackbuf.tMETRIC.ppc440.miss;

              /* update_matrix(parent->self).structures */
              for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].instruction += (sit->second).instruction;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].load        += (sit->second).load;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].store       += (sit->second).store;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].modify      += (sit->second).modify;

                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;

              }
              for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tinstruction += (sit->second).instruction;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tload        += (sit->second).load;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tstore       += (sit->second).store;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tmodify      += (sit->second).modify;

                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.thits += (sit->second).ppc440.hits;
                (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.tmiss += (sit->second).ppc440.miss;
              }
            }

            /* add last trace value */
            mainStack.top().METRIC[aType]++;
            mainStack.top().tMETRIC[aType]++;

            mainStack.top().METRIC.ppc440.hits += hits;
            mainStack.top().METRIC.ppc440.miss += miss;

            mainStack.top().tMETRIC.ppc440.hits += hits;
            mainStack.top().tMETRIC.ppc440.miss += miss;

            if(varname != ""){
              mainStack.top().METRIC.vars[varname][aType]++;
              mainStack.top().tMETRIC.vars[varname][aType]++;

              mainStack.top().METRIC.vars[varname].ppc440.hits += hits;
              mainStack.top().METRIC.vars[varname].ppc440.miss += miss;

              mainStack.top().tMETRIC.vars[varname].ppc440.tmiss += hits;
              mainStack.top().tMETRIC.vars[varname].ppc440.tmiss += miss;
            }
          }
        }
        else{
          /* stash for later */
          /* XXX */
          //std::cout << "glprof: Warning, new function starts with a heap or global access at " << itteration << "\n";
        }
      }
      else{
        /* same function */
        mainStack.top().METRIC[aType]++;
        mainStack.top().tMETRIC[aType]++;

        mainStack.top().METRIC.ppc440.hits += hits;
        mainStack.top().METRIC.ppc440.miss += miss;

        mainStack.top().tMETRIC.ppc440.hits += hits;
        mainStack.top().tMETRIC.ppc440.miss += miss;

        if(varname != ""){
          mainStack.top().METRIC.vars[varname][aType]++;
          mainStack.top().tMETRIC.vars[varname][aType]++;

          mainStack.top().METRIC.vars[varname].ppc440.hits += hits;
          mainStack.top().METRIC.vars[varname].ppc440.miss += miss;

          mainStack.top().tMETRIC.vars[varname].ppc440.hits += hits;
          mainStack.top().tMETRIC.vars[varname].ppc440.miss += miss;
        }

        /* update Stack pointer only for stack references */
        if( mainStack.top().addr > addr &&
            (segType == gleipnir::SegmentTypes::STACK || segType == gleipnir::SegmentTypes::UNKNOWN) ){
          mainStack.top().addr = addr;
        }

        /* update dynamic and global structures
         * ------------------------------------
         * At this point (Feb 11, 2014), we don't support manually tracking user stack variables
         * only heap and global types */
        //        if (segType == gleipnir::SegmentTypes::HEAP || segType == gleipnir::SegmentTypes::GLOBAL ){
        //          std::cout << "variable found: " << varname << std::endl;
        //        }
      }
    }

    itteration++;
  }

//  std::cout << "glprof: flush outstanding\n";

  /* flush outstanding */
  while(mainStack.size() > 1){
    stackbuf = mainStack.top();

//    /* Print Stack-Trace */
//    std::cout << std::right
//      << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::INSTRUCTION]
//      << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::LOAD]
//      << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::STORE]
//      << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::MODIFY]
//      << std::setw(2) << " "
//      << std::setw( mainStack.size() * 2 ) << " ";
//    std::cout << "<- " << stackbuf.fnname << std::endl;

    mainStack.pop();

    /* update_matrix(self) / .structures */
    myMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION] +=stackbuf.METRIC.instruction;
    myMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]        +=stackbuf.METRIC.load;
    myMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]       +=stackbuf.METRIC.store;
    myMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]      +=stackbuf.METRIC.modify;

    myMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
    myMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

    myMap[stackbuf.fnname].tinstruction +=stackbuf.tMETRIC.instruction;
    myMap[stackbuf.fnname].tload        +=stackbuf.tMETRIC.load;
    myMap[stackbuf.fnname].tstore       +=stackbuf.tMETRIC.store;
    myMap[stackbuf.fnname].tmodify      +=stackbuf.tMETRIC.modify;

    myMap[stackbuf.fnname].ppc440.thits += stackbuf.tMETRIC.ppc440.hits;
    myMap[stackbuf.fnname].ppc440.tmiss += stackbuf.tMETRIC.ppc440.miss;

    for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
      myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::INSTRUCTION]  += (sit->second).instruction;
      myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::LOAD]         += (sit->second).load;
      myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::STORE]        += (sit->second).store;
      myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::MODIFY]       += (sit->second).modify;

      myMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
      myMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;
    }
    for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
      myMap[stackbuf.fnname].vars[(sit->first)].tinstruction += (sit->second).instruction;
      myMap[stackbuf.fnname].vars[(sit->first)].tload        += (sit->second).load;
      myMap[stackbuf.fnname].vars[(sit->first)].tstore       += (sit->second).store;
      myMap[stackbuf.fnname].vars[(sit->first)].tmodify      += (sit->second).modify;

      myMap[stackbuf.fnname].vars[(sit->first)].ppc440.thits += (sit->second).ppc440.hits;
      myMap[stackbuf.fnname].vars[(sit->first)].ppc440.tmiss += (sit->second).ppc440.miss;
    }

    /* update stack(parent) / .structures / .gParents */
    mainStack.top().tMETRIC.instruction += stackbuf.tMETRIC.instruction;
    mainStack.top().tMETRIC.load        += stackbuf.tMETRIC.load;
    mainStack.top().tMETRIC.store       += stackbuf.tMETRIC.store;
    mainStack.top().tMETRIC.modify      += stackbuf.tMETRIC.modify;

    mainStack.top().tMETRIC.ppc440.hits += stackbuf.tMETRIC.ppc440.hits;
    mainStack.top().tMETRIC.ppc440.miss += stackbuf.tMETRIC.ppc440.miss;

    for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
      mainStack.top().tMETRIC.vars[sit->first].instruction += (sit->second).instruction;
      mainStack.top().tMETRIC.vars[sit->first].load        += (sit->second).load;
      mainStack.top().tMETRIC.vars[sit->first].store       += (sit->second).store;
      mainStack.top().tMETRIC.vars[sit->first].modify      += (sit->second).modify;

      mainStack.top().tMETRIC.vars[sit->first].ppc440.hits += (sit->second).ppc440.hits;
      mainStack.top().tMETRIC.vars[sit->first].ppc440.miss += (sit->second).ppc440.miss;
    }

    std::set<std::string>::iterator git;
    for(git = stackbuf.gParents.begin(); git != stackbuf.gParents.end(); ++git){
      myMap[stackbuf.fnname].gParents.insert(*git);
    }

    /* update matrix(parent->self) */
    it = myMap.find(mainStack.top().fnname);
    if(it == myMap.end()){
      /* not found */
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION] += stackbuf.METRIC.instruction;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]        += stackbuf.METRIC.load;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]       += stackbuf.METRIC.store;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]      += stackbuf.METRIC.modify;

      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tinstruction += stackbuf.tMETRIC.instruction;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tload        += stackbuf.tMETRIC.load;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tstore       += stackbuf.tMETRIC.store;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].tmodify      += stackbuf.tMETRIC.modify;

      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.thits += stackbuf.tMETRIC.ppc440.hits;
      myMap[mainStack.top().fnname].cMap[stackbuf.fnname].ppc440.tmiss += stackbuf.tMETRIC.ppc440.miss;

      /* update_matrix(parent->self).structures */
      for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].instruction += (sit->second).instruction;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].load        += (sit->second).load;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].store       += (sit->second).store;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].modify      += (sit->second).modify;

        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.hits += (sit->second).ppc440.hits;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.miss += (sit->second).ppc440.miss;
      }
      for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tinstruction += (sit->second).instruction;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tload        += (sit->second).load;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tstore       += (sit->second).store;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].tmodify      += (sit->second).modify;

        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.thits += (sit->second).ppc440.hits;
        myMap[mainStack.top().fnname].cMap[stackbuf.fnname].vars[sit->first].ppc440.tmiss += (sit->second).ppc440.miss;
      }
    }
    else{
      /* found */
      (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION]  += stackbuf.METRIC.instruction;
      (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]         += stackbuf.METRIC.load;
      (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]        += stackbuf.METRIC.store;
      (it->second).cMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]       += stackbuf.METRIC.modify;

      (it->second).cMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
      (it->second).cMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

      (it->second).cMap[stackbuf.fnname].tinstruction += stackbuf.tMETRIC.instruction;
      (it->second).cMap[stackbuf.fnname].tload        += stackbuf.tMETRIC.load;
      (it->second).cMap[stackbuf.fnname].tstore       += stackbuf.tMETRIC.store;
      (it->second).cMap[stackbuf.fnname].tmodify      += stackbuf.tMETRIC.modify;

      (it->second).cMap[stackbuf.fnname].ppc440.thits += stackbuf.tMETRIC.ppc440.hits;
      (it->second).cMap[stackbuf.fnname].ppc440.tmiss += stackbuf.tMETRIC.ppc440.miss;

      /* update_matrix(parent->self).structures */
      for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].instruction += (sit->second).instruction;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].load        += (sit->second).load;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].store       += (sit->second).store;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].modify      += (sit->second).modify;

        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;
      }
      for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tinstruction += (sit->second).instruction;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tload        += (sit->second).load;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tstore       += (sit->second).store;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].tmodify      += (sit->second).modify;

        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.thits;
        (it->second).cMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.tmiss;
      }
    }
  }

  stackbuf = mainStack.top();

//  /* Print Stack-Trace (outstanding) */
//  std::cout << std::right
//    << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::INSTRUCTION]
//    << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::LOAD]
//    << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::STORE]
//    << std::setw(9) << stackbuf.METRIC[gleipnir::AccessTypes::MODIFY]
//    << std::setw(3) << " "
//    << std::setw( mainStack.size() * 2 ) << " ";
//  std::cout << "(E) " << stackbuf.fnname << std::endl;
//  std::cout << std::endl << std::endl;

  mainStack.pop();

  /* update_matrix(self) */
  myMap[stackbuf.fnname][gleipnir::AccessTypes::INSTRUCTION] += stackbuf.METRIC.instruction;
  myMap[stackbuf.fnname][gleipnir::AccessTypes::LOAD]        += stackbuf.METRIC.load;
  myMap[stackbuf.fnname][gleipnir::AccessTypes::STORE]       += stackbuf.METRIC.store;
  myMap[stackbuf.fnname][gleipnir::AccessTypes::MODIFY]      += stackbuf.METRIC.modify;

  myMap[stackbuf.fnname].ppc440.hits += stackbuf.METRIC.ppc440.hits;
  myMap[stackbuf.fnname].ppc440.miss += stackbuf.METRIC.ppc440.miss;

  myMap[stackbuf.fnname].tinstruction += stackbuf.tMETRIC.instruction;
  myMap[stackbuf.fnname].tload        += stackbuf.tMETRIC.load;
  myMap[stackbuf.fnname].tstore       += stackbuf.tMETRIC.store;
  myMap[stackbuf.fnname].tmodify      += stackbuf.tMETRIC.modify;

  myMap[stackbuf.fnname].ppc440.thits += stackbuf.METRIC.ppc440.hits;
  myMap[stackbuf.fnname].ppc440.tmiss += stackbuf.METRIC.ppc440.miss;


  /* update_matrix(self).structures from stack */
  for(sit = stackbuf.METRIC.vars.begin(); sit != stackbuf.METRIC.vars.end(); ++sit){
    myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::INSTRUCTION]  += (sit->second).instruction;
    myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::LOAD]         += (sit->second).load;
    myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::STORE]        += (sit->second).store;
    myMap[stackbuf.fnname].vars[(sit->first)][gleipnir::AccessTypes::MODIFY]       += (sit->second).modify;

    myMap[stackbuf.fnname].vars[(sit->first)].ppc440.hits += (sit->second).ppc440.hits;
    myMap[stackbuf.fnname].vars[(sit->first)].ppc440.miss += (sit->second).ppc440.miss;
  }
  for(sit = stackbuf.tMETRIC.vars.begin(); sit != stackbuf.tMETRIC.vars.end(); ++sit){
    myMap[stackbuf.fnname].vars[(sit->first)].tinstruction += (sit->second).instruction;
    myMap[stackbuf.fnname].vars[(sit->first)].tload += (sit->second).load;
    myMap[stackbuf.fnname].vars[(sit->first)].tstore += (sit->second).store;
    myMap[stackbuf.fnname].vars[(sit->first)].tmodify += (sit->second).modify;

    myMap[stackbuf.fnname].vars[(sit->first)].ppc440.thits += (sit->second).ppc440.hits;
    myMap[stackbuf.fnname].vars[(sit->first)].ppc440.tmiss += (sit->second).ppc440.miss;
  }

  /* Print matrix stats */
  std::cout << "Trace profile:\n";
  int myPrintCounter = 0;
  for(it = myMap.begin(); it != myMap.end(); ++it){

    /* print title */
    if(myPrintCounter % 25 == 0){
      std::cout << std::setw(10) << "Tot Ins." << std::setw(10) << "(I %)"
        << std::setw(10) << "Tot Rd"   << std::setw(10) << "(Rd %)"
        << std::setw(10) << "Tot Wr"   << std::setw(10) << "(Wr %)"
        << std::setw(10) << "Tot Mod"  << std::setw(10) << "(Mod %)"
        << std::setw(10) << "Tot Ref"  << std::setw(10) << "(Ref %)"
        << std::setw(10) << "Tot Hit"  << std::setw(10) << "(Hit %)"
        << std::setw(10) << "Tot Mis"  << std::setw(10) << "(Mis %)"
        << "    " << "FNNAMES" << std::endl;
      std::cout << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << std::setw(10) << "-------" << std::setw(10) << "-------"
        << "    " << "-------" << std::endl;
    }

    /* Print gparent */
    std::set<std::string>::iterator git = (it->second).gParents.begin();

    std::cout \
      << std::setw(140) << "    "  \
      << " (gp)  ";
    for(/* git */ ; git != (it->second).gParents.end(); ++git){
      std::cout << *git << ", ";
    }
    std::cout << std::endl;

    /* Calculate percents */
    double instruction_p = 0.00, load_p = 0.00, store_p = 0.00, modify_p = 0.00;
    double refs_p = 0.00, hits_p = 0.00, miss_p = 0.00;

    if( (it->second).tinstruction != 0 )
      instruction_p = (it->second).instruction / (it->second).tinstruction;
    else
      instruction_p = 0.00;

    if( (it->second).tload != 0 )
      load_p = (it->second).load / (double) (it->second).tload;
    else
      load_p = 0.00;

    if( (it->second).tstore != 0 )
      store_p = (it->second).store / (double) (it->second).tstore;
    else
      store_p = 0.00;

    if( (it->second).tmodify != 0 )
      modify_p = (it->second).modify / (double) (it->second).tmodify;
    else
      modify_p = 0.00;

    if( (it->second).ppc440.thits != 0 )
      hits_p = (it->second).ppc440.hits / (double) (it->second).ppc440.thits;
    else
      hits_p = 0.00;

    if( (it->second).ppc440.tmiss != 0 )
      miss_p = (it->second).ppc440.miss / (double) (it->second).ppc440.tmiss;
    else
      miss_p = 0.00;

    if( ((it->second).ppc440.thits + (it->second).ppc440.tmiss) != 0 )
      refs_p = ((it->second).ppc440.thits + (it->second).ppc440.tmiss) / (double) ((it->second).ppc440.thits + (it->second).ppc440.tmiss);
    else
      refs_p = 0.00;

    /* Print parent */
    std::cout << std::fixed << std::setprecision(2) \
      << std::setw(10) << (it->second).tinstruction        \
      << std::setw(10) << instruction_p       \
      << std::setw(10) << (it->second).tload               \
      << std::setw(10) << load_p \
      << std::setw(10) << (it->second).tstore              \
      << std::setw(10) << store_p \
      << std::setw(10) << (it->second).tmodify             \
      << std::setw(10) << modify_p              \
      << std::setw(10) << (it->second).ppc440.thits + (it->second).ppc440.tmiss \
      << std::setw(10) << refs_p \
      << std::setw(10) << (it->second).ppc440.thits  \
      << std::setw(10) << hits_p  \
      << std::setw(10) << (it->second).ppc440.tmiss \
      << std::setw(10) << miss_p \
      << "  (p) " << it->first                    \
      << std::endl;

    /* print parent's structs */
    tmpMap = it->second.vars;
    for(sit = tmpMap.begin(); sit != tmpMap.end(); ++sit){
     
      /* Calculate percents */
      if( (sit->second).tinstruction != 0 )
        instruction_p = (sit->second).instruction / (sit->second).tinstruction;
      else
        instruction_p = 0.00;

      if( (sit->second).tload != 0 )
        load_p = (sit->second).load / (double) (sit->second).tload;
      else
        load_p = 0.00;

      if( (sit->second).tstore != 0 )
        store_p = (sit->second).store / (double) (sit->second).tstore;
      else
        store_p = 0.00;

      if( (sit->second).tmodify != 0 )
        modify_p = (sit->second).modify / (double) (sit->second).tmodify;
      else
        modify_p = 0.00;

      if( (sit->second).ppc440.thits != 0 )
        hits_p = (sit->second).ppc440.hits / (double) (sit->second).ppc440.thits;
      else
        hits_p = 0.00;

      if( (sit->second).ppc440.tmiss != 0 )
        miss_p = (sit->second).ppc440.miss / (double) (sit->second).ppc440.tmiss;
      else
        miss_p = 0.00;

      if( ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss) != 0 )
        refs_p = ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss) / (double) ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss);
      else
        refs_p = 0.00;

      std::cout \
        << std::setw(10) << (sit->second).tinstruction            \
        << std::setw(10) << (sit->second).instruction             \
        << std::setw(10) << (sit->second).tload                   \
        << std::setw(10) << (sit->second).load                    \
        << std::setw(10) << (sit->second).tstore                  \
        << std::setw(10) << (sit->second).store                   \
        << std::setw(10) << (sit->second).tmodify                 \
        << std::setw(10) << (sit->second).modify                  \
        << std::setw(10) << (sit->second).ppc440.thits + (sit->second).ppc440.tmiss \
        << std::setw(10) << refs_p \
        << std::setw(10) << (sit->second).ppc440.thits  \
        << std::setw(10) << hits_p  \
        << std::setw(10) << (sit->second).ppc440.tmiss \
        << std::setw(10) << miss_p \
        << "    s) "    << sit->first               \
        << std::endl;
    }

    std::map<std::string, counter>::iterator cit;
    tmpMap = it->second.cMap;

    /* Print children */
    for(cit = tmpMap.begin(); cit != tmpMap.end(); cit++){
      
      /* Calculate percents */
      if( (cit->second).tinstruction != 0 )
        instruction_p = (cit->second).instruction / (cit->second).tinstruction;
      else
        instruction_p = 0.00;

      if( (cit->second).tload != 0 )
        load_p = (cit->second).load / (double) (cit->second).tload;
      else
        load_p = 0.00;

      if( (cit->second).tstore != 0 )
        store_p = (cit->second).store / (double) (cit->second).tstore;
      else
        store_p = 0.00;

      if( (cit->second).tmodify != 0 )
        modify_p = (cit->second).modify / (double) (cit->second).tmodify;
      else
        modify_p = 0.00;

      if( (cit->second).ppc440.thits != 0 )
        hits_p = (cit->second).ppc440.hits / (double) (cit->second).ppc440.thits;
      else
        hits_p = 0.00;

      if( (cit->second).ppc440.tmiss != 0 )
        miss_p = (cit->second).ppc440.miss / (double) (cit->second).ppc440.tmiss;
      else
        miss_p = 0.00;

      if( ((cit->second).ppc440.thits + (cit->second).ppc440.tmiss) != 0 )
        refs_p = ((cit->second).ppc440.thits + (cit->second).ppc440.tmiss) / (double) ((cit->second).ppc440.thits + (cit->second).ppc440.tmiss);
      else
        refs_p = 0.00;

      std::cout \
        << std::setw(10) << cit->second.tinstruction  \
        << std::setw(10) << cit->second.instruction   \
        << std::setw(10) << cit->second.tload         \
        << std::setw(10) << cit->second.load          \
        << std::setw(10) << cit->second.tstore        \
        << std::setw(10) << cit->second.store         \
        << std::setw(10) << cit->second.tmodify       \
        << std::setw(10) << cit->second.modify        \
        << std::setw(10) << (cit->second).ppc440.thits + (cit->second).ppc440.tmiss \
        << std::setw(10) << refs_p \
        << std::setw(10) << (cit->second).ppc440.thits  \
        << std::setw(10) << hits_p  \
        << std::setw(10) << (cit->second).ppc440.tmiss \
        << std::setw(10) << miss_p \
        << "  (c)  " << cit->first                   \
        << std::endl;

      /* print structs */
      for(sit = (cit->second).vars.begin(); sit != (cit->second).vars.end(); ++sit){
        /* Calculate percents */
        if( (sit->second).tinstruction != 0 )
          instruction_p = (sit->second).instruction / (sit->second).tinstruction;
        else
          instruction_p = 0.00;

        if( (sit->second).tload != 0 )
          load_p = (sit->second).load / (double) (sit->second).tload;
        else
          load_p = 0.00;

        if( (sit->second).tstore != 0 )
          store_p = (sit->second).store / (double) (sit->second).tstore;
        else
          store_p = 0.00;

        if( (sit->second).tmodify != 0 )
          modify_p = (sit->second).modify / (double) (sit->second).tmodify;
        else
          modify_p = 0.00;

        if( (sit->second).ppc440.thits != 0 )
          hits_p = (sit->second).ppc440.hits / (double) (sit->second).ppc440.thits;
        else
          hits_p = 0.00;

        if( (sit->second).ppc440.tmiss != 0 )
          miss_p = (sit->second).ppc440.miss / (double) (sit->second).ppc440.tmiss;
        else
          miss_p = 0.00;

        if( ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss) != 0 )
          refs_p = ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss) / (double) ((sit->second).ppc440.thits + (sit->second).ppc440.tmiss);
        else
          refs_p = 0.00;

        std::cout \
          << std::setw(10) << (sit->second).tinstruction     \
          << std::setw(10) << (sit->second).instruction      \
          << std::setw(10) << (sit->second).tload            \
          << std::setw(10) << (sit->second).load             \
          << std::setw(10) << (sit->second).tstore           \
          << std::setw(10) << (sit->second).store            \
          << std::setw(10) << (sit->second).tmodify          \
          << std::setw(10) << (sit->second).modify           \
          << std::setw(10) << (sit->second).ppc440.thits + (sit->second).ppc440.tmiss \
          << std::setw(10) << refs_p \
          << std::setw(10) << (sit->second).ppc440.thits  \
          << std::setw(10) << hits_p \
          << std::setw(10) << (sit->second).ppc440.tmiss \
          << std::setw(10) << miss_p \
          << "      s) "  << sit->first                \
          << std::endl;
      }
    }
    std::cout << std::endl;

    myPrintCounter++;
  }

  return (0);
}
