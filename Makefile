CXXFLAGS=-Wall -std=c++0x -O3
LIBS=-L./simulator/ -lolcfmemres-simulator

all: glprof

parser.o: parser.cxx parser.h
	g++ $(CXXFLAGS) -c -O0 parser.cxx

glprof: glprof.cxx parser.o
	g++ -I./simulator -L./simulator -lolcfmemres-simulators ./simulator/ppc440.o ./simulator/statistics.o $(CXXFLAGS) $? -O0 -o $@

clean:
	rm -f *.o glprof
