#include "parser.h"

#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string.h>
#include <set>
#include <stack>
#include <map>
#include <vector>

/* gleipnir Record (aka. trace line); class methods */
/* constructors */
  gleipnir::Record::Record(uintptr_t aAddress, const std::string* aOwnerFunction, size_t aSize, int aThreadId)
: mAccessType(AccessTypes::INSTRUCTION),
  mAddress(aAddress),
  mSize(aSize),
  mThreadId(aThreadId),
  mAccessScope(AccessScopes::UNKNOWN,-1),
  mOwnerFunction(aOwnerFunction)
{ 
  mUnion0.mSegmentType = SegmentTypes::UNKNOWN;
}

gleipnir::Record::Record(
    uintptr_t aAddress, const std::string* aOwnerFunction, size_t aSize, int aThreadId,
    AccessTypes::Value aAccessType,
    SegmentTypes::Value aSegmentType,
    const std::pair<AccessScopes::Value,int>& aAccessScope,
    const std::string* aVarName,
    unsigned long long aVarValue,
    unsigned long long aCycle
    ) 
: mAccessType(aAccessType),
  mAddress(aAddress),
  mSize(aSize),
  mThreadId(aThreadId),
  mAccessScope(aAccessScope),
  mOwnerFunction(aOwnerFunction),
  mVarName(aVarName),
  mVarValue(aVarValue),
  mCycle(aCycle)
{ 
  mUnion0.mSegmentType = aSegmentType;
}

gleipnir::Record::Record(unsigned long long aInstructionCount, Metadata::Value aId, 
    const std::string* aObjectName, size_t aSize, uintptr_t aAddress) 
: mAccessType(AccessTypes::METADATA),
  mAddress(aAddress),
  mSize(aSize),
  mVarName(aObjectName),
  mCycle(aInstructionCount)
{ 
  mUnion0.mMetadataId = aId;
}

gleipnir::Record gleipnir::Record::obtain(const std::string& aLine,
    std::set<std::string>& aStrings){
  return RecordParser().obtain(aLine, aStrings);
}

/* retrieve trace line */
gleipnir::Record gleipnir::RecordParser::obtain(const std::string& aLine,
    std::set<std::string>& aStrings){

  // const char *s = "I 00040080a 2 1 S main\n";
  // const char *s = "S 0051bc2c0 8 1 H main H-0 hello_c_32.656 [0x4037800000000000]";

  static int sIndex=-1;    // will be replaced eventually
  char lAccessTypeChar=-1;
  AccessTypes::Value lAccessType = AccessTypes::UNKNOWN;
  unsigned long long lAddress;
  size_t lSize;
  size_t lThreadId;
  SegmentTypes::Value lSegment = SegmentTypes::UNKNOWN;


  mSS.clear();
  mSS.str("");
  mSS << aLine;

  sIndex++;

  /* Access type */		
  mSS >> lAccessTypeChar;
  switch (lAccessTypeChar) {
    case 'I' : { lAccessType = AccessTypes::INSTRUCTION; break; }
    case 'L' : { lAccessType = AccessTypes::LOAD; break; }
    case 'S' : { lAccessType = AccessTypes::STORE; break; }
    case 'M' : { lAccessType = AccessTypes::MODIFY; break; }
    case 'X' : { lAccessType = AccessTypes::METADATA; break; }
    default: { throw (EMalformed()); }
  };
  mTmp.clear();

  /* Are these trace keywords? */	
  if (lAccessType == AccessTypes::METADATA) {
    mSS >> std::skipws >> mTmp;
    if (mTmp=="UPDATE") {      
      mSS >> std::skipws >> mTmp;
      if (mTmp != "MSTRUCT") {
        throw (EMalformed());
      }


      std::string lVarName;
      size_t lSize = 0;
      uintptr_t lAddress;

      mSS >> std::skipws >> lVarName;
      // skip the "DOES NOT EXIST!" message
      if (lVarName == "DOES") {
        mSS >> std::skipws >> mTmp >> std::skipws >> mTmp >> std::skipws >> lVarName;
        mSS >> std::skipws >> mTmp >> std::hex >> lAddress;
      } else {
        mSS >> std::skipws >> lSize >> std::skipws >> mTmp >> std::skipws >> std::hex >> lAddress;
      }

      return (Record(0, Metadata::UPDATE_MSTRUCT, & *aStrings.insert(lVarName).first, lSize, lAddress));
    } else if (mTmp=="MARK") {
      std::string lName;
      unsigned long long lInstructionCount;
      mSS >> std::skipws >> lName >> std::skipws >> lInstructionCount;
      return (Record(lInstructionCount, Metadata::MARK, & *aStrings.insert(lName).first));
    }

    //TODO: parse the rest of the X bits
    return (Record());
  }

  /* Address and access size */
  mSS << std::hex;
  mSS >> std::skipws >> lAddress;
  mSS << std::dec;
  mSS >> lSize >> lThreadId >> std::skipws >> mTmp;  

  /* success, what type of access is it? */
  if (mTmp.size()==1) {
    if (!mIsCudaStream) {
      switch (mTmp[0]) {
        case 'S' : { lSegment=SegmentTypes::STACK; break; }
        case 'G' : { lSegment=SegmentTypes::GLOBAL; break; }
        case 'H' : { lSegment=SegmentTypes::HEAP; break; }
      }
    } else {
      switch (mTmp[0]) {
        case 'L' : { lSegment=SegmentTypes::CUDA_LOCAL; break; }
        case 'G' : { lSegment=SegmentTypes::CUDA_GLOBAL; break; }
        case 'S' : { lSegment=SegmentTypes::CUDA_SHARED; break; }
        case 'C' : { lSegment=SegmentTypes::CUDA_CONSTANT; break; }
        case 'T' : { lSegment=SegmentTypes::CUDA_TEXTURE; break; }
      }
    }
  }

  /* Basic trace_line complete, look for debug info */

  std::set<std::string>::const_iterator lStringLookup = aStrings.find(mTmp);
#if 0
  if (lStringLookup != aStrings.end()) {
    std::cout << "** string located: " << (void*) & *lStringLookup << std::endl;
  }
#endif

  /* Function if !(instruction) */
  std::string lFunction;
  if (lAccessType==AccessTypes::INSTRUCTION) {
    lFunction=mTmp;

    return (Record(lAddress, & *aStrings.insert(lFunction).first, lSize, lThreadId));
  } else {
    mSS >> std::skipws >> lFunction;
  }

  std::pair<AccessScopes::Value,int> lAccessScope(AccessScopes::UNKNOWN,-1);
  if (!mIsCudaStream) {
    std::string lScopeSpecifier;
    std::string lVarDesc;
    unsigned long long lVarValue = 0;

    mSS >> lScopeSpecifier;
    if (!mSS.fail()) {
      if (lScopeSpecifier=="LS") {
        lAccessScope.first = AccessScopes::LOCAL_STRUCTURE;
      } else if (lScopeSpecifier=="LV") {
        lAccessScope.first = AccessScopes::LOCAL_VARIABLE;
      } else if (lScopeSpecifier=="GS") {
        lAccessScope.first = AccessScopes::GLOBAL_STRUCTURE;
      } else if (lScopeSpecifier=="GV") {
        lAccessScope.first = AccessScopes::GLOBAL_VARIABLE;
      } else if (lScopeSpecifier[0]=='H') {
        sscanf(lScopeSpecifier.c_str(), "H-%d", &lAccessScope.second);
        lAccessScope.first = AccessScopes::HEAP_ALLOC;
      }

      char lChar;
      mSS >> std::skipws >> lVarDesc;
      mSS >> std::skipws >> lChar >> lVarValue >> lChar;

      /* Clear structure offset
       * XXX we may need that later */
      lVarDesc = lVarDesc.substr(0, lVarDesc.find("."));
      
      lVarDesc = lVarDesc.substr(0, lVarDesc.find("["));
    }

    return (Record(lAddress, & *aStrings.insert(lFunction).first, lSize, lThreadId,
          lAccessType, lSegment, lAccessScope,
          lAccessScope.first!=AccessScopes::UNKNOWN ? & *aStrings.insert(lVarDesc).first : NULL,
          lVarValue));
  } else {
    // in CUDA streams, the function name is succeeded by the cycle; there is no var value yet
    unsigned long long lCycle;
    mSS >> lCycle;
    return (Record(lAddress, & *aStrings.insert(lFunction).first, lSize, lThreadId,
          lAccessType, lSegment, lAccessScope, 
          (const std::string*) NULL, 0LLU, lCycle));
  }
}

std::ostream& operator<<(std::ostream& os, const gleipnir::Record& rd)
{
  os << rd.ownerFunction();
  return os;
}

gleipnir::RecordReader::RecordReader(const char* aPath, bool aIsCudaStream) : RecordParser(aIsCudaStream) {
  mInput.open(aPath);
  if (!mInput.is_open()) {
    throw (EFailed());
  }
  mHasNext = true;
  advance();
}

void gleipnir::RecordReader::advance() {
  if (!mHasNext) {
    return;
  }
  mHasNext = false;
next:
  if (mInput.eof()) {
    return;
  }

  mLine.clear();
  std::getline(mInput, mLine);

  if (mLine.size()==0 || !::isalpha(mLine[0])) {
    goto next;
  }

  mHasNext = true;
}

gleipnir::Record gleipnir::RecordReader::next() {
  if (!hasNext()) {
    throw (EFailed());
  }
  gleipnir::Record lRecord(obtain(mLine, mAllStringsEncountered));
  advance();
  return (lRecord);
}

gleipnir::Record gleipnir::FilteringRecordReader::next() {
  if (!hasNext()) throw(RecordReader::EFailed());
  mChecked = false;
  return (mBuffered);
}

void gleipnir::FilteringRecordReader::advance() {

  mHasNext = false;
  mChecked = true;
  while (mRecordReader.hasNext()) {
    mBuffered = mRecordReader.next();
    if ((mBuffered.getAccessType() & mAccessTypesMask) != 0) {
      mHasNext = true;
      return;
    }
  }
}

gleipnir::StringsTable::StringsTable() {
  mStrings = std::pair<char*,size_t>((char*) malloc(128*1024), 128*1024);
  mNextOffset=0;
  mSize=0;
  mSealed=false;
}

gleipnir::StringsTable::StringsTable(const char* aBuffer, size_t aLength) {
  mSize=aLength;
  mStrings = std::pair<char*,size_t>((char*)aBuffer, 0);

  size_t lLastOffset=0;

  for (size_t lI=0 ; lI<aLength ; lI++) {
    if (!aBuffer[lI]) {
      mIndexToOffset[mIndexToOffset.size()] = lLastOffset;
      lLastOffset = lI+1;
    }
  }
  mSealed=true;
}

gleipnir::StringsTable::StringsTable(const char* aFilePath) {
  FILE *lFile = fopen(aFilePath,"r");
  if (lFile==NULL) {
    throw (ENotFound());
  }
  fseek(lFile, 0, SEEK_END);
  mSize = ftell(lFile);
  fseek(lFile, 0, SEEK_SET);
  mStrings = std::pair<char*,size_t>((char*)malloc(mSize), mSize);
  fread(mStrings.first, mSize, 1, lFile);  

  size_t lLastOffset=0;

  for (size_t lI=0 ; lI<mSize ; lI++) {
    if (!mStrings.first[lI]) {
      mIndexToOffset[mIndexToOffset.size()] = lLastOffset;
      lLastOffset = lI+1;
    }
  }
  mSealed=true;

  fclose(lFile);
}

gleipnir::StringsTable::~StringsTable() {
  if (mStrings.second>0) { 
    delete[] mStrings.first; 
  }
}

size_t gleipnir::StringsTable::insert(const std::string& aString) {
  if (mSealed) {
    throw (ESealed());
  }
  auto lResult = mStringToIndex.insert(std::pair<std::string, size_t>(aString, mStringToIndex.size()));
  if (lResult.second) {
    size_t lNextOffset = mNextOffset+aString.size()+1;
    if (lNextOffset < mStrings.second) {
      char *lNew = (char*)realloc(mStrings.first, mStrings.second*2);
      mStrings = std::pair<char*,size_t>(lNew, mStrings.second*2);
    }    
    memcpy(mStrings.first+mNextOffset, aString.c_str(), aString.size()+1);    

    mIndexToOffset[mIndexToOffset.size()] = mNextOffset;
    mSize += aString.size()+1;
    mNextOffset += aString.size()+1;

  }
  return (lResult.first->second);
}

const char* gleipnir::StringsTable::base() const { return (mStrings.first); }

const char* gleipnir::StringsTable::operator[](size_t aKey) const {
  if (!mSealed) {
    throw (ENotSealed());
  }
  if (aKey >= size()) {
    throw (ENotFound());
  }
  return (base() + mIndexToOffset.find(aKey)->second);
}

void gleipnir::StringsTable::seal() { 
  mStringToIndex.clear();
  mSealed=true; 
}

void gleipnir::StringsTable::dump() const {
  for (size_t lI=0 ; lI<size() ; lI++) {
    size_t lOffset = mIndexToOffset.find(lI)->second;
    std::cout << std::setw(5) << lI << " " << std::setw(12) << lOffset << " " << (*this)[lI] << std::endl;
  }
}

extern "C" void* gleipnir_stringstable_create() {
  return (new gleipnir::StringsTable());
}

extern "C" int   gleipnir_stringstable_put(void* aTable, const char* aString) {
  return (((gleipnir::StringsTable*) aTable)->insert(aString));
}

extern "C" void  gleipnir_stringstable_store_and_destroy(void *aTable, FILE* aOut) {
  gleipnir::StringsTable* lTable = (gleipnir::StringsTable*)aTable;
  lTable->seal();
  fwrite((const void*) lTable->base(), lTable->sizeInBytes(), 1, aOut);
  delete lTable;
}
